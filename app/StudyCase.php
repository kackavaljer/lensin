<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudyCase extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_id', 'created_by', 'name', 'year', 'category', 'country_id', 'source', 'producer', 'author','designer', 'location', 'main_contact', 'email', 'website', 'status', 'start_year', 'description', 'benefits', 'published', 'gps', 'other_notes', 'language'
    ];


	/**
	 * Created by
	 */
	public function created_by()
	{
	    return $this->belongsTo('App\User', 'created_by');
	}

	/**
	 * Guidelines
	 */
	public function guidelines()
	{
	    return $this->belongsToMany('App\StudyCaseGuideline')->orderBy('level', 'order');
	}


	/**
	 * Guidelines level 0
	 */
	public function guidelines_level_0()
	{
	    return $this->belongsToMany('App\StudyCaseGuideline')->where('level', 0)->orderBy('order');
	}



	/**
     * Country
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }


}
