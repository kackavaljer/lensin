<?php    
    
    function name_to_text($element)
    {
        return trans('text.'.$element);
    }


	/**
	 * RETURN CURRENT LANGUAGE
	 */
	function cur_lan(){
		if (Session::get('locale')) {
			return Session::get('locale');
		}
		if (Auth::check()) {
			return Auth::user()->default_language;
		} 
		return 'en';
	}

	/**
	 * RETURN CURRENT LANGUAGE NAME
	 */
	function cur_lan_name(){
		$language = \App\Language::where('locale', 'LIKE', cur_lan())->first();
		if ($language) {
			return $language->name;	
		} else {
			return 'No languages';
		}
		
	}


	/**
	 * Curent user cover image 
	 * @return link to the image
	 */
	function user_image($user_id = null) {
		if (!$user_id) {
			if (Auth::check()) {
				if (Auth::user()->image){
					return Auth::user()->image;
				} else {
					return '/images/user-profiles/default_logged_in.png';
				}
			} else {
				return '/images/user-profiles/default.png';
			}
		} else {
			$user = \App\User::find($user_id);
			if ($user) {
				if ($user->image){
					return $user->image;
				} else {
					return '/images/user-profiles/default.png';
				}
			} else {
				return '/images/user-profiles/default.png';
			}
		}
	}


	/**
	 * Server property
	 * @param  string $name - name of the property
	 * @return string 		- the data form the property or false
	 */
	function server_property($name) {
		$data = \App\ServerProperties::where('name', 'LIKE', $name)->first();
		if ($data->data) {
			return $data->data;	
		} else {
			return false;
		}
	}

	/**
	 * Social icons
	 * @return links to images
	 */
	function social_icons() {
		
		// get the property from server properties and decode
		$icons = json_decode(server_property('social_icons'));

		$link = "";

		foreach ($icons as $key => $value) {
			// if there is a value, add the icon
			if ($value){
				$link .= '<a href="' . $value . '"><img src="/images/social_icons/' . $key . '.png" class="social_icons"></a>';
			}
		}

		return $link;
	}
	

	/**
	 * Return role by slug
	 */
	function role($slug)
	{
		return Role::where('slug', $slug)->first();
	}	



	function view_td($title, $text) {
		$title = strtoupper(trans('text.' . $title));
		$return_data = "
			<tr>
				<td>
					<!-- type -->
					<span class='title'>
						$title
					</span>
				</td>
				<td class='text-right'>
					$text
				</td>
			</tr>";

		return $return_data;
	}


	/***** LISTS *****/

	// COURSE - schools
	function list_schools() {
		// TODO - add other tables
		$return_data = DB::table('courses')->select('school')->distinct()->get();
		return $return_data;
	}
	
	// STUDY CASE - categories	
	function list_study_case($filed) {
		$return_data = DB::table('study_cases')->select($filed)->distinct()->get();
		return $return_data;
	}

	// DATA FROM CENTRAL SERVER
	function central_server_data($url) {

		// make a url
		$central_server_url = \App\ServerProperties::where('name', 'LIKE', 'central_server_address')->first();

		$context = stream_context_create(array('http' => array('ignore_errors' => true)));

		$return_data = @file_get_contents($central_server_url->data . $url, false, $context);

		return $return_data;	
		
	}

	
	// LIST OF ALL PLATFORMS
	function platforms_list() {
		
		// decode the list from central server
		$platforms_list = json_decode(central_server_data('/api/server_list'));
		
		$platforms = [];

		// get platform addreses
		if ($platforms_list) {
			foreach ($platforms_list as $platform) {
				array_push($platforms, $platform->address);
			}
		}

		// return the array
		return $platforms;
	}

	/** return if there is index **/
	function echo_index($array, $index){
		if (isset($array[$index])) {
			return $array[$index];
		} else {
			return "";
		}
	}


	/**
	 * Server URL from id
	 * @param  int 		$server_id 		Id of the server 
	 * @return string 					Server url
	 */
	function server_url($server_id)
	{
		if ($server_id) {

			// get the server from list
			$server = \App\ServerList::find($server_id);
			
			// if no server found
			if (!$server) {
				flash(trans('text.no_server_found'));
    			return redirect('/');
			}			

			// RETURN the server address
			return $server->address;
		
		} else {
			
			// Local view server address
			return url("/");
		}
	}


	function tooltip($text, $class = "")
	{
		$return_data = '<i class="fa fa-question-circle-o tooltip_text" aria-hidden="true" title="' . $text . '"></i>';
		return $return_data;
	}

?>