<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lecture extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'course_subject_id', 'name', 'order', 'description', 'image', 'published'
    ];

	/**
     * Course
     */
    public function course_subject()
    {
        return $this->belongsTo('App\CourseSubject');
    }

	/**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }


    /**
     * Get all of the lectures resources.
     */
    public function resources()
    {
        return $this->morphMany('App\Resource', 'resourcable');
    }

    /**
     * Get all of the lectures resources.
     */
    public function resources_published()
    {
        return $this->morphMany('App\Resource', 'resourcable')->where('published', 1);
    }    


    /**
     * Contents
     */
    public function contents()
    {
        return $this->hasMany('App\LectureContent');
    }



    //observe this model being deleted and delete the child events
    public static function boot()
    {
        parent::boot();

        self::deleting(function (Lecture $lecture) {

            foreach ($lecture->resources as $resource)
            {
                $resource->delete();
            }

            foreach ($lecture->contents as $content)
            {
                $content->delete();
            }

            
        });
    }

}
