<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'server_id', 'country_id', 'language_id', 'name', 'image', 'school', 'duration', 'type', 'description', 'year'
    ];


	/**
     * Country
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }


	/**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }


	/**
     * Teachers 
     */
    public function teachers()
    {
        return $this->belongsToMany('App\User', 'course_teacher');
    }


    /**
     * Language 
     */
    public function language()
    {
        return $this->belongsTo('App\Language');
    }


    /**
     * Course Subjects
     */
    public function subjects()
    {
        return $this->hasMany('App\CourseSubject')->orderBy('order');
    }



    /**
     * Course Subjects published
     */
    public function subjects_published()
    {
        return $this->hasMany('App\CourseSubject')->orderBy('order')->where('published', 1);
    }    



    //observe this model being deleted and delete the child events
    public static function boot ()
    {
        parent::boot();

        self::deleting(function (Course $course) {

            foreach ($course->subjects as $subject)
            {
                $subject->delete();
            }
        });
    }


}