<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LectureContent extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lecture_id', 'created_by', 'text'
    ];


	/**
     * Lecture parrent
     */
    public function lecture()
    {
        return $this->belongsTo('App\Lecture');
    }


	/**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

}
