<?php

namespace App\Http\Middleware;

use Closure, Config, App;
use Illuminate\Support\Facades\Session;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //)->setLocale(Session::get('locale'));
        //app()->setLocale(Session::get('locale'));
        $locale = Session::get('locale');
        App::setLocale($locale);

        return $next($request);
    }
}
