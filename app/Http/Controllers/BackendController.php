<?php

namespace App\Http\Controllers;

use Image;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BackendController extends Controller
{
	
	/**
	 * Front of backend / USERS
	 */
    public function backend()
    {
    	// if user has one of admin groups enter backend
    	if (Auth::user()->hasGroup('admin')){ 

    		return view('backend.welcome');

    	} else {
    		// else go to home
    		flash(trans('text.not_authorised'))->important();
    		return redirect(url('/login'));
    	}

    }


// USERS

    /**
     * User list
     */
    public function users_list()
    {
   		return view('backend.users');
    }

    /**
     * Edit user
     */
	public function user_edit(Request $request, User $user)
	{
        // Get all coutries
		$countries = \App\Country::all()->pluck('name', 'id');
		// Get all user types
		$user_types = \App\UserType::all()->pluck('name', 'id');

		// update type names
		foreach ($user_types as $user_type) {
			
		}

		$user_priviledges = \HttpOz\Roles\Models\Role::all();

		return view('backend.users_edit', ['user' => $user, 'countries' => $countries, 'user_types' => $user_types, 'user_priviledges' => $user_priviledges]);
	}


    /**
     * SERVER PROPERTIES
     */
    public function site()
    {
        $properties = \App\ServerProperties::all();
        return view('backend.site', ['properties' => $properties]);
    }


    /**
     * UPDATE SITE PROPERTIES
     */
    public function site_update(Request $request)
    {
        
        $property = \App\ServerProperties::where('name', 'LIKE', $request->name)->first();
        $property->data = $request->data;
        $property->save();

        flash(trans('text.saved'))->important();
        return back();
    }



    public function upload_logo(Request $request, $path)
    {
        // Validate the request
        $this->validate($request, ['image' => 'required']);

        if (!file_exists(public_path() . '/images/server/')) {
            mkdir(public_path() . '/images/server/', 0777, true);
        }

        // read image from temporary file
        $img = Image::make($request->image);

        if ($path == 'logo') {

            $image_path = '/images/logo.png';    

        } elseif ($path == 'round_logo') {

            $image_path = '/images/server/logo.png';

        } elseif ($path == 'round_logo_transparent') {

            $image_path = '/images/server/logo_transparent.png'; 

        }
        

        // save image
        $img->save(public_path() . $image_path);

        flash(trans('text.logo_updated'))->important();
        return back();
    }

    /**
     * Register a platform, return languages
     * @param  Request $request server_address
     * @return json           server_id, languages, translations
     */
    public function register_platform(Request $request)
    {
        // vaidate the request
        $this->validate($request, ['server_address' => 'required']);

        // get the server srom the list if it is registered
        $server_registering = \App\ServerList::where('address', 'LIKE', $request->server_address)->first();
        // on error return 
        if (!$server_registering) {
            return "no_such_server";
        }
        // put the id in return data
        $return_data = ['server_id' => $server_registering->id];

        // get the languages 
        $languages = \App\Language::get()->toArray();
        $return_data += ['languages' => $languages];

        // get the translations
        $translations = \App\LanguageTranslation::all()->toArray();
        $return_data += ['translations' => $translations];

        return $return_data;
    }


    /**
     * Request for registering the platform on the central server
     */
    public function register_platform_request()
    {
        // make the request url
        $central_server_url = server_property('central_server_address') . "/backend/register_platform?server_address=" . url('/');

        // get the data from the central server
        if (!$data = json_decode(file_get_contents($central_server_url))){
            // return the error
            flash(trans('backend.error_geting_data_from_central_server') . $central_server_url )->important();
            return back();
        }

        // set server id
        $server_id = \App\ServerProperties::where('name', 'LIKE', 'server_id')->first();
        $server_id->data = $data->server_id;
        $server_id->save();

        // LANGAUGES
        if ($data->languages && $data->translations) {

            // Delete old translations
            $old_translations = \App\LanguageTranslation::where('id', '>', 0)->forceDelete();

            // delete old languages
            $old_languages = \App\Language::where('id', '>', 0)->forceDelete();
            
            // create new languages
            foreach ($data->languages as $language) {
                \App\Language::create([
                        'id' => $language->id, 
                        'name' => $language->name, 
                        'locale' => $language->locale, 
                        'published' => $language->published
                        ]);
            }

            // create new translations
            foreach ($data->translations as $translation) {
                \App\LanguageTranslation::create([
                        'id' => $translation->id, 
                        'locale' => $translation->locale, 
                        'namespace' => $translation->namespace, 
                        'group' => $translation->group, 
                        'item' => $translation->item, 
                        'text' => $translation->text, 
                        'unstable' => $translation->unstable, 
                        'locked' => $translation->locked
                        ]);
            }

        }
        
        // clear the cache
        \Artisan::call('cache:clear');

        flash(trans('backend.server_connected_translations_pulled'));
        return back();

    }


    /**
     * Sycnhronize languages and translations
     */
    public function synchronize(Request $request)
    {

        // check if the request is from 
        if ($request->central_server_address != server_property('central_server_address')) {
            return "| " . url('/') . ": ERROR | ";
        }

        // make the request url
        $central_server_url = server_property('central_server_address') . "/backend/register_platform?server_address=" . url('/');

         // get the data from the central server
        if (!$data = json_decode(file_get_contents($central_server_url))){
            // return the error
            flash(trans('backend.error_geting_data_from_central_server') . $central_server_url )->important();
            return back();
        }        

        // LANGAUGES
        if ($data->languages && $data->translations) {

            // Delete old translations
            $old_translations = \App\LanguageTranslation::where('id', '>', 0)->forceDelete();

            // delete old languages
            $old_languages = \App\Language::where('id', '>', 0)->forceDelete();
            
            // create new languages
            foreach ($data->languages as $language) {
                \App\Language::create([
                        'id' => $language->id, 
                        'name' => $language->name, 
                        'locale' => $language->locale, 
                        'published' => $language->published
                        ]);
            }

            // create new translations
            foreach ($data->translations as $translation) {
                \App\LanguageTranslation::create([
                        'id' => $translation->id, 
                        'locale' => $translation->locale, 
                        'namespace' => $translation->namespace, 
                        'group' => $translation->group, 
                        'item' => $translation->item, 
                        'text' => $translation->text, 
                        'unstable' => $translation->unstable, 
                        'locked' => $translation->locked
                        ]);
            }

        }
        
        // clear the cache
        \Artisan::call('cache:clear');

        return "| " . url('/') . ": OK | ";

    }
    
}
