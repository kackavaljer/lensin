<?php

namespace App\Http\Controllers;

use Datatables;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function all_resources_array(Request $request)
    {
    	// get the requests form the user
		$resources = isset($request->resources) ? $request->resources : ['course', 'lecture', 'study_case', 'tool'];
		$platforms = $request->platforms ? $request->platforms : [];

		//create an empty collection for data
		$data = new \Illuminate\Database\Eloquent\Collection;
		
		// Courses
		$array_course = array_search('course', $resources);
    	if ($array_course || $array_course === 0) {
			$courses = app('App\Http\Controllers\CourseController')->table_data(1);
			if ($courses) {
				$data = $courses->merge($data);    
			}
    	}

		// Lectures
		$array_lectures = array_search('lecture', $resources);
    	if ($array_lectures || $array_lectures === 0) {
			$lectures = app('App\Http\Controllers\LectureController')->table_data(1);
			if ($lectures) {
				$data = $lectures->merge($data);
			}
    	}		

		// study cases
		$array_study_cases = array_search('study_case', $resources);
    	if ($array_study_cases || $array_study_cases === 0) {
			$study_cases = app('App\Http\Controllers\StudyCaseController')->table_data(1);
			if ($study_cases) {
				$data = $study_cases->merge($data);
			}
    	}		


        // tools
        $array_tools = array_search('tool', $resources);
        if ($array_tools || $array_tools === 0) {
            $tools = app('App\Http\Controllers\ToolController')->table_data(1);
            if ($tools) {
                $data = $tools->merge($data);
            }
        }   

    	return $data;
    }


    /**
     * All Resources in datatable
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function all_resources_datatable(Request $request){

    	$data = $this->all_resources_array($request);
    	// return the data
		return Datatables::of($data)->make();
    }



    /**
     * All resources form all servers
     */
    public function all_resources_all_servers(Request $request)
    {
    	$data = [];
    	$succ = "";
    	// get all servers
    	$servers = \App\ServerList::where('enabled', 1)->get();

    	//for each server get data 
    	foreach ($servers as $server) {

    		// get the data and test if return
    		if ($server_data = @file_get_contents($server->address . "/all_resources")){
    			// test if json and decode
    			if ($server_data_array = json_decode($server_data, true)){
    				// merge the data
    				$data = array_merge($data, $server_data_array);
    			}
    		}
    	}

    	// return collected data
    	return $data;
    	
    }

    /**
     * All Resources all servers in datatable
     */
    public function all_resources_all_servers_datatables(Request $request){

    	$data = collect($this->all_resources_all_servers($request));
    	// return the data
		return Datatables::of($data)->make();
    }    

}
