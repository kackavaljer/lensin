<?php

namespace App\Http\Controllers;

use Image;
use App\User;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Datatable list
     * @return Collection 
     */
    public function datatable_list()
    {
		$data = User::all();
		return Datatables::of($data)->make(true);
    }


	/**
	 * Update from input fields
	 */
    public function update_data_backend(Request $request)
	{	
		$name = $request->name;
		
		// Update the Course
		$user = User::find($request->id);
		$user->$name = $request->value;
		$user->save();

		return 'OK';

    }


    /**
     * Update roles
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update_roles(Request $request, User $user)
    {
    	if ($request->value == 1) {
   	        $user->attachRole($request->id);
	   	} else {
	   		$user->detachRole($request->id);
	   	}
    	
		return $request;
    }

    /**
     * User profile page
     */
    public function profile()
    {
        $user = Auth::user();
        $countries = \App\Country::all()->pluck('name', 'id');
        $user_types = \App\UserType::all()->pluck('name', 'id');        

        return (view('users.profile', ['user' => $user, 'countries' => $countries, 'user_types' => $user_types]));
    }


    /**
     * Upload user image
     */
    public function upload_image(Request $request, User $user)
    {

        // Validate the request
        $this->validate($request, ['image' => 'required|image']);

        // read image from temporary file
        $img = Image::make($request->image);

        // resize image
        $img->resize(800, 800, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });;


        $image_path = '/images/users/' . $user->id . '/' . $user->id . "_" . time() . '.jpg';

        if (!file_exists(public_path() . '/images/users/' . $user->id)) {
            mkdir(public_path() . '/images/users/' . $user->id, 0777, true);
        }

        // save image
        $img->save(public_path() . $image_path);

        $user->image = url($image_path);
        $user->save();

        $returnData = view('courses.course_cover_image', ['course' => $user]);
        return $returnData;
    }


    /**
     * Remove user profile image
     * @return OK
     */
    public function remove_image(Request $request, User $user)
    {
        $user->image = "";
        $user->save();
        return 'OK';
    }



}
