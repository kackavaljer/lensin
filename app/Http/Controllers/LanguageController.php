<?php

namespace App\Http\Controllers;

use Session;
use App\Language;
use App\LanguageTranslation;
use Illuminate\Http\Request;

class LanguageController extends Controller
{

	/**
	 * LIST OF LANGUAGES
	 */
    public function index()
    {
    	// get all languages
    	$languages = Language::all();
    	$platforms = \App\ServerList::where('enabled', 1)->get();

    	// return the view
		return view('backend.languages_index', ['languages' => $languages, 'platforms' => $platforms]);
    }

    /**
     * CREATE THE LANGUAGE
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request)
    {

		// Validate the request
		$this->validate($request, [
				'locale' => 'required',
				'name' => 'required',
			]);

		// Create the language
		$language = Language::create($request->all());  

		// copy English translations
		$translations = LanguageTranslation::where('locale', 'LIKE', 'en')->get();

		foreach ($translations as $translation) {
			LanguageTranslation::create([
					'locale' => $language->locale,
					'namespace' => $translation->namespace,
					'group' => $translation->group,
					'item' => $translation->item,
					'text' => $translation->text,
					'unstable' => $translation->unstable,
					'locked' => $translation->locked
				]);
		}

		$language->save();

		flash( trans('text.created') )->important();
		return back();


    }


    /**
     * UPDATE THE LANGUAGE
     */
	public function update(Request $request, Language $language)
	{
		$language->published = null;
		$language->update($request->all());

		flash( trans('text.updated') )->important();
		return back();    	
	}


	/**
	 * MANAGE
	 */
	public function manage(Request $request, Language $language)
	{

		return view('backend.languages_index', ['translations' => $translations]);
	}


	/**
	 * Change Locale
	 */
	public function change_locale(Request $request, $locale)
	{
		Session::put('locale', $locale);
		\Artisan::call('cache:clear');
		return back();
	}


	/**
	 * Delete the language
	 */
	public function delete(Request $request, Language $language)
	{
		// delete the language
		$language->forceDelete();

		// flash the message to the user
		flash(trans('text.deleted'))->important();

		// clear te cache
		\Artisan::call('cache:clear');

		// return back
		return back();

	}




}
