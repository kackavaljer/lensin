<?php

namespace App\Http\Controllers;

use Datatables;
use App\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ThemesController extends Controller
{

	/**
	 * CREATE VIEW
	 */
    public function create_view()
    {
    	$view = view('themes.create');
    	return $view;
    }


	/**
	 * CREATE THEME
	 */
    public function create(Request $request)
    {
		// Validate the request
		$this->validate($request, ['name' => 'required']);

		// Add the aditional settings
		$additional_data = [
			'created_by' => Auth::user()->id, 
			'server_id' => server_property('server_id'),
			];

		// Create the tool
		$theme = Theme::create($request->all() + $additional_data);

		flash( trans('text.created') )->important();
		return redirect("/projects/themes/edit/" . $theme->id);

    }

    /**
     * EDIT THEME
     */
    public function edit(Request $request, Theme $theme)
    {
    	return view('themes.edit', ['theme' => $theme]);
    }


	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		
		// Update the Course
		$theme = Theme::find($request->id);
		$theme->$name = $request->value;
		$theme->save();

		return 'OK';
	}

	/**
	 * List of themes to modify
	 */
	public function modify()
	{
    	return view('themes.modify');
	}


	/**
	 * Data table for modify
	 */
	public function data_table_modify()
	{
		$data = Theme::all();
		return Datatables::of($data)->make(true);
	}


	/**
	 * Delete the theme
	 */
	public function delete(Request $request, Theme $theme)
	{
		// delete the theme
		$theme->delete();
		// send the text
		flash(trans('text.deleted'))->important();
		// return back
		return back();
	}


}
