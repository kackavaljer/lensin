<?php

namespace App\Http\Controllers;

use Image;
use Storage;
use App\Tool;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ToolController extends Controller
{

	/**
	 * CREATE TOOL VIEW
	 * @return view 
	 */
    public function create_view()
    {
    	$category_list = [];
    	return view('tools.create', ['category_list' => $category_list]);
    }


	/**
	 * CREATE TOOL
	 * @return view 
	 */
    public function create(Request $request)
    {

		// Validate the request
		$this->validate($request, ['title' => 'required']);

		// Add the aditional settings
		$additional_data = [
			'created_by' => Auth::user()->id, 
			'category' => $request->category, 
			'server_id' => server_property('server_id'),
			];

		// Create the tool
		$tool = Tool::create($request->all() + $additional_data);

		// Save the file if exists
		if ($request->file) {
			//set the path
			$path = 'resources/tools/' . $tool->id;
			
			// save the file
			Storage::disk('public')->putFileAs($path, $request->file, $request->file->getClientOriginalName());

			// update the tool
			$tool->filename = url($path . "/" . $request->file->getClientOriginalName());
			$tool->save();
		}

		flash( trans('text.created') )->important();
		return redirect("/tools/edit/" . $tool->id);

    }


    /**
     * MODIFY VIEW
     */
	public function modify(Request $request)
	{
		return view('tools.modify');
	}


	/**
     * Tools datatable list
     */
    public function datatable_list(Request $request) {

    	$data = $this->table_data();
		return Datatables::of($data)->make(true);

    }


    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
    	// get tools
		if ($published == true) {
			$tools = \App\Tool::where('published', 1)->get();
		} else {
			$tools = \App\Tool::all();
		}

		if (!$tools->count() > 0) {
			return false;
		}
		
		// create a data array
		$data = $tools->map(function ($item) {
			
			$author = \App\User::find($item->created_by);

		    return [
	    		'id' => $item->id, 
	    		'title' => $item->title, 
	    		'author' => $author->name . " " . $author->last_name, 
	    		'country' => "", 
	    		'year' => "", 
	    		'language' => "", 
	    		'type' => 'tool', 
	    		'platform' => str_replace('http://', "", url("/")),
	    		'platform_id' => server_property('server_id'),
	    		'published' => $item->published,
	    		'category' => $item->category,
	    		'description' => $item->description,
	   		];
		});

		return $data;
    }


	/**
	 * PUBLISH
	 */
	public function publish(Request $request, Tool $tool)
	{
		$tool->published = 1;
		$tool->save();

		flash(trans('text.published'))->important();
		return back();
	}


	/**
	 * UNPUBLISH
	 */
	public function unpublish(Request $request, Tool $tool)
	{
		$tool->published = null;
		$tool->save();
		
		flash(trans('text.unpublished'))->important();
		return back();
	}



    /**
     * EDIT TOOLS PAGE
     */
    public function edit(Request $request, Tool $tool)
    {
    	$category_list = [];
    	return view('tools.edit', ['tool' => $tool, 'category_list' => $category_list]);
    }


	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		
		// Update the Course
		$tool = Tool::find($request->id);
		$tool->$name = $request->value;
		$tool->save();

		return 'OK';
	}


	/**
	 * UPLOAD FILE
	 */
	public function upload_file(Request $request, Tool $tool)
	{

		//set the path
		$path = 'resources/tools/' . $tool->id;
		
		// save the file
		Storage::disk('public')->putFileAs($path, $request->filename, $request->filename->getClientOriginalName());

		// update the tool
		$tool->filename = url($path . "/" . $request->filename->getClientOriginalName());
		$tool->save();

		return $tool->filename;
	}


	public function delete(Request $request, Tool $tool)
	{
		// delete the tool
		$tool->delete();

		// flash the message
		flash( trans('text.deleted') )->important();
		return back();
	}


    /**
     * VIEW THE COURSE
     */
    public function view(Request $request, $tool_id)
    {

        $server_url = server_url($request->server_id); 
        
        // GET DATA FROM SERVER
        $tool = json_decode(file_get_contents($server_url . "/tools/view/api/" . $tool_id), true);
        
        // return courses view
        return view('tools.view', ['tool' => $tool[0]]);
    }


    /**
     * API DATA FOR COURSE VIEW
     */
	public function view_api(Request $request, $tool_id)
	{
		$tool = Tool::where('id', $tool_id)->with('created_by')->get();
		return $tool;
	}



	/**
	 * UPDATE COURSE IMAGE
	 */
	public function upload_cover_image(Request $request, Tool $tool)
	{
		// Handle image 
		if ($request->image) {
			
			// read image from temporary file
			$img = Image::make($request->image);

			// resize image
			$img->resize(800, 800, function ($c) {
				    $c->aspectRatio();
				    $c->upsize();
				});;

			$image_path = '/images/tools/' . $tool->id . '/' . $tool->id . "_" . time() . '.jpg';
			
			// Make folder if not exist
			if (!file_exists(public_path() . '/images/tools/' . $tool->id)) {
			    mkdir(public_path() . '/images/tools/' . $tool->id, 0777, true);
			}

			// save image
			$img->save(public_path() . $image_path);

			$tool->image = url('/') . $image_path;
			$tool->save();
		}

		// return image path
		return $tool->image;
	}


	/**
	 * REMOVE COVER IMAGE
	 */
	public function remove_cover_image(Request $request, Tool $tool)
	{
		$tool->image = "";
		$tool->save();

		return "OK";
	}

}
