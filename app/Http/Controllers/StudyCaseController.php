<?php

namespace App\Http\Controllers;

use Image;
use App\StudyCase;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class StudyCaseController extends Controller
{
	/**
	 * DISPLAYS THE CREATE STUDY CASE PAGE
	 */
    public function createPage()
    {
		$countries = \App\Country::all()->pluck('name', 'id');
		$languages = \App\Language::all()->pluck('name', 'id');

    	return view('study_cases.create', ['countries' => $countries, 'languages' => $languages]);
    }


    /**
     * Create study case
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request)
    {
    	//Validate the request
		$this->validate($request, ['name' => 'required']);

		// Add the aditional settings
		$additional_data = ['created_by' => Auth::user()->id, 'server_id' => server_property('server_id')];

		// Create the study case
		$study_case = StudyCase::create($request->all() + $additional_data);

		// Handle image 
		if ($request->image) {
			
			// read image from temporary file
			$img = Image::make($request->image);

			// resize image
			$img->resize(800, 800, function ($c) {
				    $c->aspectRatio();
				    $c->upsize();
				});;


			$image_path = '/images/study_cases/' . $study_case->id . '/' . $study_case->id . "_" . time() . '.jpg';

			if (!file_exists(public_path() . '/images/study_cases/' . $study_case->id)) {
			    mkdir(public_path() . '/images/study_cases/' . $study_case->id, 0777, true);
			}

			// save image
			$img->save(public_path() . $image_path);

			$study_case->image = $image_path;
		}

		// save she study case
		$study_case->save();

		// flash the massage
		flash(trans('text.study_case_created'))->important();

		// redirect to edit page
		return redirect('/study_cases/edit/' . $study_case->id);

    }


    /**
     * MODIFY LIST
     */
	public function modify(Request $request)
	{
		return view('study_cases.modify');
	}

	
	/**
     * All srudy cases list
     */
    public function datatable_list(Request $request) {

    	$data = $this->table_data();
		return Datatables::of($data)->make(true);

    }


    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
    	// get courses
		if ($published == true) {
			$study_cases = \App\StudyCase::where('published', 1)->get();
		} else {
			$study_cases = \App\StudyCase::all();
		}

		if (!$study_cases->count() > 0) {
			return false;
		}
		
		// create a data array
		$data = $study_cases->map(function ($item) {
		
		    return [
		    		'id' => $item->id,
		    		'title' => $item->name, 
		    		'author' => $item->producer, 
		    		'country' => "", //$item->country->name, 
		    		'year' => $item->year, 
		    		'language' => $item->language, 
		    		'type' => 'study_case',
		    		'platform' => str_replace('http://', "", url("/")),
		    		'published' => $item->published,
		    		'description' => $item->description, 
		    		'category' => $item->category,
		    		'state' => $item->state
	    		];
		});

		return $data;
    }


	/**
	 * PUBLISH
	 */
	public function publish(Request $request, StudyCase $study_case)
	{
		$study_case->published = 1;
		$study_case->save();

		flash(trans('text.published'))->important();
		return back();
	}


	/**
	 * UNPUBLISH
	 */
	public function unpublish(Request $request, StudyCase $study_case)
	{
		$study_case->published = null;
		$study_case->save();
		
		flash(trans('text.unpublished'))->important();
		return back();
	}	

	/**
	 * DELETE STUDY CASE
	 */
	public function delete(Request $request, StudyCase $study_case)
	{
		$study_case->delete();

		flash(trans('text.study_case_deleted'))->important();
		return back();
	}


    /**
     * DISPLAY THE EDIT STUDY CASE PAGE
     */
    public function edit(Request $request, StudyCase $study_case)
    {
		$countries = \App\Country::all()->pluck('name', 'id');
		$languages = \App\Language::all()->pluck('name', 'id');    	
    	$guidelines = \App\StudyCaseGuideline::orderBy('level', 'order')->where('level', 0)->get();

    	return view('study_cases.edit', ['guidelines' => $guidelines, 'study_case' => $study_case, 'countries' => $countries, 'languages' => $languages]);
    }


	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		
		// Update the Course
		$study_case = StudyCase::find($request->id);
		$study_case->$name = $request->value;
		$study_case->save();

		return 'OK';
	}


	/**
	 * Remove image from study case 
	 * @return OK
	 */
	public function remove_image(Request $request, StudyCase $study_case)
	{
		$study_case->image = "";
		$study_case->save();
		return 'OK';
	}


	/**
	 * STUDY CASE VIEW
	 */
	public function view(Request $request, $study_case_id)
	{

		// GET DATA FROM SERVER
		if ($request->server_id) {
			
			// get the server from list
			$server = \App\ServerList::find($request->server_id);

			// if no server found return to home
			if (!$server) {
				flash(trans('text.no_server_found'))->important();
    			return redirect('/');
			}

			$server_url = $server->address;

		} else {

			// local view
			$server_url = url("/");
		}

		// get the study case data
		$study_case = json_decode(file_get_contents($server_url . "/study_cases/view/api/" . $study_case_id));
	
		// return study case view		
		return view('study_cases.view', ['study_case' => $study_case[0]]);
	}


    /**
     * API DATA FOR STUDY CASE VIEW
     */
	public function view_api(Request $request, $study_case_id)
	{
		$study_case = StudyCase::where('id', $study_case_id)->with('guidelines_level_0', 'guidelines_level_0.children', 'guidelines_level_0.children.children', 'guidelines_level_0.children.children.children')->get();
		return $study_case;
	}


	/**
	 * UPDATE COVER IMAGE
	 */
	public function cover_image_upload(Request $request, StudyCase $study_case)
	{
		// Handle image 
		if ($request->image) {
			
			// read image from temporary file
			$img = Image::make($request->image);

			// resize image
			$img->resize(800, 800, function ($c) {
				    $c->aspectRatio();
				    $c->upsize();
				});;

			$image_path = '/images/courses/' . $study_case->id . '/cover/' . $study_case->id . "_" . time() . '.jpg';
			
			// Make folder if not exist
			if (!file_exists(public_path() . '/images/courses/' . $study_case->id . '/cover')) {
			    mkdir(public_path() . '/images/courses/' . $study_case->id . '/cover', 0777, true);
			}

			// save image
			$img->save(public_path() . $image_path);

			$study_case->image = $image_path;
			$study_case->save();
		}

		// redirect back
		$returnData = view('study_cases.cover_image', ['study_case' => $study_case]);
		return $returnData;
	}



	/**
	 * UPDATE REPORT
	 */
	public function report_upload(Request $request, StudyCase $study_case)
	{

		$this->validate($request, ['report_file' => 'file|required']);

		// Handle file 
		$file_path = '/study_cases/' . $study_case->id . '/report';
	
		Log::info(public_path() . $file_path);

		// Make folder if not exist
		if (!file_exists(public_path() . $file_path)) {
		    mkdir(public_path() . $file_path, 0777, true);
		}


		// save the file
		Storage::disk('public')->putFileAs($file_path, $request->report_file, $request->report_file->getClientOriginalName());

		// update the resource
		$study_case->report = url($file_path . "/" . $request->report_file->getClientOriginalName());
		$study_case->save();

		// redirect back
		
		$return_data = '<a href="' . $study_case->report .'" download target="_blank"> ' . $study_case->report .'</a>';
		return $return_data;
	}


	/**
	 * Delete the report forn the study case
	 */
	public function report_delete(Request $request)
	{
		$study_case = StudyCase::find($request->id);
		if ($study_case) {
			$study_case->report = "";
			$study_case->save();
		}

		return "OK";
	}


}
