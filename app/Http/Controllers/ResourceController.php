<?php

namespace App\Http\Controllers;

use App\Lecture;
use Storage;
use App\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ResourceController extends Controller
{
	/**
     * Create Resource
     * @return int 	course subject id
     */
	public function create(Request $request, Lecture $lecture)
	{
		$orderResource = $lecture->resources()->orderBy('order', 'desc')->first();
		$order = 1;
		if (!$orderResource) {
			$order = 1;
		} else {
			$order = $orderResource->order + 1;
		}

		Log::info('=====' . $lecture->id);

		$resource = Resource::create([
			'created_by' => Auth::user()->id, 
			'resourcable_id' => $lecture->id, 
			'resourcable_type' => 'App\Lecture', 
			'order' => $order,
			'created_at' => null, 
			'updated_at' => null, 
		]);

		/*
 		if ($this->upload_file($request, $resource)) {
 			$view = view('courses.resource_edit', ['resource' => $resource])->render();
 			$return_data = [$resource->id, $view];
	        return $return_data;
 		} else {
 			
 		}
 		*/
 		return $resource;
	}


	/**
	 * Upload resource file
	 */
	public function upload_file(Request $request, Resource $resource)
	{
		// TODO CHECK FILE SIZE !!!
		
		//set the path
		$path = 'resources/' . substr($resource->resourcable_type, 4) . "/" . $resource->resourcable_id;
		
		// save the file
		Storage::disk('public')->putFileAs($path, $request->resource_file, $request->resource_file->getClientOriginalName());

		$filename = url($path . "/" . $request->resource_file->getClientOriginalName());
		
		// UPDATE THE RESOURCE
		
		// Decode the old filenames or make a new array
		if ($resource->path) {
			$file_list = json_decode($resource->path, true);
		} else {
			$file_list = [];
		}

		array_push($file_list, $filename);

	    // encode the data and save to the database
		$resource->path = json_encode($file_list);
		$resource->save();


		if ($request->type == "grid") {
 			$view = view('courses.resource_edit', ['resource' => $resource])->render();
 			$return_data = [$resource->id, $view];
	        return $return_data;
	    }

		return "OK";
	}


	public function lecture_modal_file_list(Request $request, Resource $resource)
	{
		$view = view('courses.edit_resource_modal_file_list', ['resource' => $resource]);
		return ($view);
	}

	/**
	 * CREATE A RESOUCE AND UPLOAD A FILE
	 * @param  Request $request 
	 * @param  Lecture $lecture 
	 * @return view
	 */
	public function create_and_upload_files(Request $request, Lecture $lecture)
	{
		
		// create the resource
		$resource = $this->create($request, $lecture);

		// set the uplad path
		$path = 'resources/' . strtolower(substr($resource->resourcable_type, 4)) . "/" . $resource->id;

		// list of files
		$file_list = [];


		// save files to the server
		foreach ($request->resource_file as $resource_file) {

			// save the file
			Storage::disk('public')->putFileAs($path, $resource_file, $resource_file->getClientOriginalName());

			// make an absolute file path
			$absolute_file_path = url('/') . "/" . $path . "/" . $resource_file->getClientOriginalName();

			// add to the file list
			array_push($file_list, $absolute_file_path);
		}

		// update the resource
		$resource->path = json_encode($file_list);
		$resource->save();
		
		// return the view
		$view = view('courses.resource_edit', ['resource' => $resource])->render();
 		$return_data = [$resource->id, $view];
	    return $return_data;

	}




	/**
	 * Delete lecture
	 * @return [text]
	 */
	public function delete(Request $request)
	{	
		// get the resource
		$resource = Resource::find($request->id);

		// if exists, delete it
		if ($resource) {
			$resource->delete();
			return trans('text.resource_deleted');
		} else {
			return trans('text.resource_does_not_exist');
		}
	}	


	public function edit_modal(Request $request, Resource $resource)
	{
		return view('courses.edit_resource_modal', ['resource' => $resource]);
	}
	


	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		// find the Resource
		$resource = Resource::find($request->id);

		if (!$resource) {
			return trans('text.resource_doesnt_exist');
		}

		// Update the Resource
		$resource->$name = $request->value;
		$resource->save();

		return 'OK';
	}





	/**
	 * LICENCE update
	 */
	public function licence_update(Request $request)
	{
		$resource = Resource::find($request->id);
		//return $request->id_belongs_to;
		if ($request->value) {
			if (!$resource->licences->contains($request->id_belongs_to)) {
			    $resource->licences()->attach($request->id_belongs_to);
			}
			return "attach";

		} else {
			$resource->licences()->detach($request->id_belongs_to);
			return "detach";
		}
	}	



	/**
	 * Publish / unpublish resource
	 * @return text		icon for the button
	 */
	public function publish(Request $request)
	{
		// get the resource
		$resource = Resource::find($request->id);

		// UNPUBLISH
		if ($resource->published == 1) {
			$resource->published = null;
			$return_icon = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
		} else {
			// PUBLISH IF RESOURCE TYPE
			if ($resource->resource_type_id != null) {
				$resource->published = 1;
				$return_icon = '<i class="fa fa-eye" aria-hidden="true"></i>';
			} else {
				$return_icon = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
			}
		}

		// save the resouce
		$resource->save();

		return ($return_icon);
	}

	/**
	 * Display modal view of the resource
	 */
	public function view_modal(Request $request, $resource_id)
	{
		// SERVER URL
		$server_url = $request->server_url; 
		
		// GET DATA FROM SERVER
		if ($server_data = @file_get_contents($server_url . "/resources/view_modal/api/" . $resource_id)){
			// decode the resource data 
			$resource = json_decode($server_data);	

		} else {
			return "NO RESOURCE!";
		}

		// return the resource modal view
		return view('courses.resource_view_modal', ['resource' => $resource[0]]);

	}


    /**
     * API DATA FOR RESOURCE VIEW
     */
	public function view_modal_api(Request $request, $resource_id)
	{
		$resource = Resource::where('id', $resource_id)->with('resourcable', 'resource_type', 'licences')->get();
		return $resource;
	}


	/**
	 * Remove resource file
	 */
	public function delete_file(Request $request, Resource $resource)
	{
		// decode form database
		$files = json_decode($resource->path);

		// delete the file from the list
	    unset($files[$request->file_index]);

	    // save the new file list
	    $resource->path = json_encode($files);
	    $resource->save();

		return view('courses.edit_resource_modal_file_list', ['resource' => $resource]);
	}


}
