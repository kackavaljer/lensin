<?php

namespace App\Http\Controllers;

use Image;
use App\Course;
use App\CourseSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseSubjectController extends Controller
{
    /**
     * Create subject
     * @return int 	course subject id
     */
	public function create(Request $request, Course $course)
	{
		$orderSubject = CourseSubject::where('course_id', $course->id)->orderBy('order', 'desc')->first();
		if (!$orderSubject) {
			$order = 1;
		} else {
			$order = $orderSubject->order + 1;
		}

		$courseSubject = CourseSubject::create(['created_by' => Auth::user()->id, 'course_id' => $course->id, 'order' => $order]);

        return view('courses.course_subject_edit', ['courseSubject' => $courseSubject]);
	}

	/**
	 * Delete course subject
	 * @return [text]
	 */
	public function delete(Request $request)
	{
		// find the subject
		$course_subject = CourseSubject::find($request->id);

		// if subject exits 
		if ($course_subject) {

			// delete the subject
			$course_subject->delete();

			// return info
			return trans('text.course_subject_deleted');

		} else {
			// if course does not exist return message 
			return trans('text.course_subject_doesnt_exist');
		}
	}


	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		// find the Course
		$courseSubject = CourseSubject::find($request->id);

		if (!$courseSubject) {
			return trans('text.course_subject_doesnt_exist');
		}

		// Update the Course
		$courseSubject->$name = $request->value;
		$courseSubject->save();

		return 'OK';
	}


	/**
	 * UPDATE THE SORTING
	 * @param  Request $request | token and array
	 */
	public function update_sort(Request $request)
	{
		$this->validate($request, ['subjects' => 'required']);

		$i = 1;
		foreach ($request->subjects as $subject) {
			$subject_object = CourseSubject::find($subject);
			$subject_object->order = $i;
			$subject_object->save();
			$i++;
		}

		return "OK";
	}

	/**
	 * Publish / unpublish course subject
	 * @return text           icon for the button
	 */
	public function publish(Request $request)
	{

		$course_subject = CourseSubject::find($request->id);

		if ($course_subject->published == 1) {
			$course_subject->published = null;
			$return_icon = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
		} else {
			$course_subject->published = 1;
			$return_icon = '<i class="fa fa-eye" aria-hidden="true"></i>';
		}

		$course_subject->save();

		return ($return_icon);
	}

}
