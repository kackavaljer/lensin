<?php

namespace App\Http\Controllers;

use App\Language;
use App\LanguageTranslation;
use Illuminate\Http\Request;

class LanguageTranslationsController extends Controller
{
  	/**
	 * LIST OF TRANSLATIONS
	 */
    public function index(Request $request, Language $language)
    {

    	// get all translations
    	$translations = LanguageTranslation::where('locale', 'LIKE', $language->locale)->orderBy('group')->get();
    	// return the view
		return view('backend.language_translations_index', ['language' => $language, 'translations' => $translations]);

    }

    /**
     * UPDATE THE LANGUAGE
     */
	public function update(Request $request, LanguageTranslation $translation)
	{
		// update the translation
		$translation->update($request->all());

		// clear the cache
		\Artisan::call('cache:clear');

		// flash the message and return back
		flash( trans('text.updated') )->important();
		return back();    	
	}



}
