<?php

namespace App\Http\Controllers;

use Image;
use App\Lecture;
use App\CourseSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class LectureController extends Controller
{
	/**
     * Create lecture
     * @return int 	course subject id
     */
	public function create(Request $request, CourseSubject $course_subject)
	{
		$orderLecture = Lecture::where('course_subject_id', $course_subject->id)->orderBy('order', 'desc')->first();
		if (!$orderLecture) {
			$order = 1;
		} else {
			$order = $orderLecture->order + 1;
		}

		$lecture = Lecture::create(['created_by' => Auth::user()->id, 'course_subject_id' => $course_subject->id, 'order' => $order]);

		if (isset($request->grid)) {
			return view('courses.edit_grid_view_subject_lecture', ['lecture' => $lecture]);	
		} else {
			return view('courses.lecture_edit', ['lecture' => $lecture]);	
		}
        
	}




	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		// find the Lecture
		$lecture = Lecture::find($request->id);

		if (!$lecture) {
			return trans('text.lecture_doesnt_exist');
		}

		// Update the Lecture
		$lecture->$name = $request->value;
		$lecture->save();

		return 'OK';
	}

	/**
	 * Delete lecture
	 * @return [text]
	 */
	public function delete(Request $request)
	{
		$lecture = Lecture::find($request->id);
		if ($lecture) {
			$lecture->delete();
			return trans('text.lecture_deleted');
		} else {
			return trans('text.lecture_doesnt_exist');
		}
	}	


	/**
	 * Upload cover image
	 */
	public function upload_cover_image(Request $request, Lecture $lecture)
	{
		// Handle image 
		if ($request->image) {
			
			// read image from temporary file
            $image = Input::file('image');

			$image_path = '/images/courses/lectures/cover/lecture_' . $lecture->id . "_" . time() . '.jpg';
			$full_path = public_path() . $image_path;

			$img = Image::make($image->getRealPath())->resize(800, 800, function ($c) {
				    $c->aspectRatio();
				    $c->upsize();
				})->save($full_path);

			$lecture->image = url('/') . $image_path;
			$lecture->save();

			return $lecture->image;
		}

		return "?";
	}


	/**
	 * View lecture
	 */
	public function view(Request $request, $lecture_id)
	{
		// Get the server url
		$server_url = server_url($request->server_id); 
		
		// GET DATA FROM SERVER
		if ($server_data = @file_get_contents($server_url . "/lectures/view/api/" . $lecture_id)){

			$lecture = json_decode($server_data);	

		} else {

			flash(trans('text.no_server_found'))->important();
			return redirect('/');
		}

		// return the lecture view
		return view('courses.lecture_view', ['lecture' => $lecture[0], 'server_id' => $request->server_id]);
	}

    /**
     * API DATA FOR LECTURE VIEW
     */
	public function view_api(Request $request, $lecture_id)
	{
		$lecture = Lecture::where('id', $lecture_id)->with('contents', 'course_subject', 'course_subject.course', 'resources_published', 'resources_published.resource_type')->get();
		return $lecture;
	}



	/**
	 * Publish / unpublish lecture 
	 * @return text           icon for the button
	 */
	public function publish(Request $request)
	{

		$lecture = Lecture::find($request->id);

		if ($lecture->published == 1) {
			$lecture->published = null;
			$return_icon = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
		} else {
			$lecture->published = 1;
			$return_icon = '<i class="fa fa-eye" aria-hidden="true"></i>';
		}

		$lecture->save();

		return ($return_icon);
	}


	/**
	 * UPDATE THE SORTING
	 * @param  Request $request | token and array
	 */
	public function update_sort(Request $request)
	{
		$this->validate($request, ['lectures' => 'required']);

		$i = 1;
		foreach ($request->lectures as $lecture) {
			$lecture_object = Lecture::find($lecture);
			$lecture_object->order = $i;
			$lecture_object->save();
			$i++;
		}

		return "OK";
	}


 /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
    	// get courses
		if ($published == true) {
			$lectures = \App\Lecture::where('published', 1)->get();
		} else {
			$lectures = \App\Lecture::all();
		}

		if (!$lectures->count() > 0) {
			return false;
		}

		// create a data array
		$data = $lectures->map(function ($item) {

			// add authors
			$author = [];
			if (isset($item->course_subject->course->teachers)) {
			
				foreach ($item->course_subject->course->teachers as $teacher) {
					array_push($author, $teacher->name . ' ' . $teacher->last_name);
				}
				$author = implode(", ", $author);
			
			} else {

				$author = "";
			}

		    return ['id' => $item->id, 
		    		'title' => $item->name, 
		    		'author' => $item->author, 
		    		'country' => (isset($item->course_subject->course->country->name) ? $item->course_subject->course->country->name : ""), 
		    		'year' => (isset($item->course_subject->course->year) ? $item->course_subject->course->year : ""), 
		    		'language' => (isset($item->course_subject->course->language->name) ? $item->course_subject->course->language->name : ""), 
		    		'type' => 'lecture', 
		    		'platform' => str_replace('http://', "", url("/")),
		    		'published' => $item->published,
	    		];
		});

		return $data;
    }


    /**
     * Lecture Edit Modal
     * @param  Request $request
     * @param  Lecture $lecture
     * @return view
     */
    public function edit_modal(Request $request, Lecture $lecture)
    {
    	return view('courses.edit_grid_view_lecture_modal', ['lecture' => $lecture]);
    }


    /**
     * Lecture grid edit view
     */
    public function grid_view_edit(Request $request, Lecture $lecture)
    {
    	return view('courses.edit_grid_view_subject_lecture', ['lecture' => $lecture]);
    }


}
