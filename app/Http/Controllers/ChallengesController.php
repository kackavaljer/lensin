<?php

namespace App\Http\Controllers;

use Datatables;
use App\Theme;
use App\Challenge;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ChallengesController extends Controller
{
	
	/**
	 * CREATE VIEW
	 */
    public function create_view()
    {
    	$themes = Theme::get()->pluck('name', 'id');
    	$view = view('challenges.create', ['themes' => $themes]);
    	return $view;
    }


	/**
	 * CREATE THEME
	 */
    public function create(Request $request)
    {
		// Validate the request
		$this->validate($request, ['name' => 'required']);

		// Add the aditional settings
		
		$additional_data = [
			'created_by' => Auth::user()->id
			];

		// Create the tool
		$challenge = Challenge::create($request->all() + $additional_data);

		flash( trans('text.created') )->important();
		return redirect("/projects/challenges/edit/" . $challenge->id);

    }


            //$table->tinyInteger('comments_enabled');


	/**
	 * List of challenges to modify
	 */
	public function modify()
	{
    	return view('challenges.modify');
	}

	/**
	 * Data table for modify
	 */
	public function data_table_modify()
	{
		$challenges = Challenge::all();

		// create a data array
		$data = $challenges->map(function ($item) {

		    return [
		    		'id' => $item->id, 
		    		'theme' => $item->theme->name, 
		    		'name' => $item->name, 
		    		'course' => $item->course, 
		    		'year' => $item->year, 
		    		'published' => $item->published, 
	    		];
		});

		return Datatables::of($data)->make(true);
	}

	/**
	 * Delete the challenge
	 */
	public function delete(Request $request, Challenge $challenge)
	{
		// delete the challenge
		$challenge->delete();
		// send the text
		flash(trans('text.deleted'))->important();
		// return back
		return back();
	}


    /**
     * EDIT THEME
     */
    public function edit(Request $request, Challenge $challenge)
    {
    	$themes = Theme::get()->pluck('name', 'id');
    	return view('challenges.edit', ['challenge' => $challenge, 'themes' => $themes]);
    }


}
