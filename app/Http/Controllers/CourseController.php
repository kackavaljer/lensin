<?php

namespace App\Http\Controllers;


use Image;
use App\Course;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{

	/**
	 * CREATE COURSE
	 */
	public function create(Request $request)
	{
		// Validate the request
		//$this->validate($request, ['name' => 'required']);

		// Add the aditional settings
		$additional_data = [
			'created_by' => Auth::user()->id, 
			'server_id' => server_property('server_id'),
			'country_id' => Auth::user()->country->id, 
			'language_id' => 1, 
			'school' => Auth::user()->school, 
			];

		// Create the Course
		$course = Course::create($request->all() + $additional_data);

		// Handle image 
		if ($request->image) {
			
			// read image from temporary file
			$img = Image::make($request->image);

			// resize image
			$img->resize(800, 800, function ($c) {
				    $c->aspectRatio();
				    $c->upsize();
				});;


			$image_path = '/images/courses/' . $course->id . '/' . $course->id . "_" . time() . '.jpg';

			if (!file_exists(public_path() . '/images/courses/' . $course->id)) {
			    mkdir(public_path() . '/images/courses/' . $course->id, 0777, true);
			}

			// save image
			$img->save(public_path() . $image_path);

			$course->image = public_path() . $image_path;
		}


		// Add teachers
		if ($request->authors) {
			$course->teachers()->attach($request->authors);
		}
		
		// save changes
		$course->save();

		// Flash the message
		//flash(trans('text.course_created'))->important();

		$countries = \App\Country::all()->pluck('name', 'id');
		$languages = \App\Language::all()->pluck('name', 'id');    	
    	
    	return view('courses.edit', ['course' => $course, 'countries' => $countries, 'languages' => $languages, 'mode' => 'create']);

		// redirect to edit page
		//return redirect('/courses/edit/' . $course->id . '?create=1');

	}



	/**
	 * DISPLAYS THE CREATE COURSE PAGE
	 */
    public function createPage()
    {
		$countries = \App\Country::all()->pluck('name', 'id');
		$languages = \App\Language::all()->pluck('name', 'id');

    	return view('courses.create', ['countries' => $countries, 'languages' => $languages]);
    }


    /**
     * VIEW THE COURSE
     */
	public function view(Request $request, $course_id)
	{

		$server_url = server_url($request->server_id); 
		
		// GET DATA FROM SERVER
		$course = json_decode(file_get_contents($server_url . "/courses/view/api/" . $course_id), true);
		
		// return courses view
		return view('courses.view', ['course' => $course[0]]);
	}



    /**
     * API DATA FOR COURSE VIEW
     */
	public function view_api(Request $request, $course_id)
	{
		$course = Course::where('id', $course_id)->with('language', 'teachers', 'country', 'subjects_published', 'subjects_published.lectures_published', 'subjects_published.lectures_published.resources_published', 'subjects_published.lectures_published.resources_published.resource_type')->get();
		return $course;
	}


    /**
     * EDIT COURSE PAGE
     */
    public function edit(Request $request, Course $course)
    {
		$countries = \App\Country::all()->pluck('name', 'id');
		$languages = \App\Language::all()->pluck('name', 'id');    	
    	
    	return view('courses.edit', ['course' => $course, 'countries' => $countries, 'languages' => $languages]);
    }


    /**
     * EDIT COURSE LIST VIEW
     */
    public function edit_list_view(Request $request, Course $course)
    {
		return view('courses.edit_list_view', ['course' => $course]);
    }

    /**
     * EDIT COURSE GRID VIEW
     */
    public function edit_grid_view(Request $request, Course $course)
    {
		return view('courses.edit_grid_view', ['course' => $course]);
    }



	/**
	 * UPDATE COURSE IMAGE
	 */
	public function update_image(Request $request, Course $course)
	{
		// Handle image 
		if ($request->image) {
			
			// read image from temporary file
			$img = Image::make($request->image);

			// resize image
			$img->resize(800, 800, function ($c) {
				    $c->aspectRatio();
				    $c->upsize();
				});;

			$image_path = '/images/courses/' . $course->id . '/' . $course->id . "_" . time() . '.jpg';
			
			// Make folder if not exist
			if (!file_exists(public_path() . '/images/courses/' . $course->id)) {
			    mkdir(public_path() . '/images/courses/' . $course->id, 0777, true);
			}

			// save image
			$img->save(public_path() . $image_path);

			$course->image = url('/') . $image_path;
			$course->save();
		}

		// redirect back
		$returnData = view('courses.course_cover_image', ['course' => $course]);
		return $returnData;
	}

	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		
		// Update the Course
		$course = Course::find($request->id);
		$course->$name = $request->value;
		$course->save();

		return 'OK';
	}

    /**
     * MODIFY LIST
     */
	public function modify(Request $request)
	{
		return view('courses.modify');
	}


	/**
	 * PUBLISH
	 */
	public function publish(Request $request, Course $course)
	{
		$course->published = 1;
		$course->save();

		flash(trans('text.published'))->important();
		return back();
	}


	/**
	 * UNPUBLISH
	 */
	public function unpublish(Request $request, Course $course)
	{
		$course->published = null;
		$course->save();
		
		flash(trans('text.unpublished'))->important();
		return back();
	}


    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {

    	// get courses
		if ($published == true) {
			$courses = \App\Course::where('published', 1)->get();
		} else {
			$courses = \App\Course::all();
		}

		if (!$courses->count() > 0) {
			return false;
		}

		// create a data array
		$data = $courses->map(function ($item) {

			// add authors
			$author = [];
			foreach ($item->teachers as $teacher) {
				array_push($author, $teacher->name . ' ' . $teacher->last_name);
			}
			$author = implode(", ", $author);
			$language_name = (($item->language) ? $item->language->name : "");

		    return [
		    		'id' => $item->id, 
		    		'title' => $item->name, 
		    		'author' => $author, 
		    		'country' => $item->country->name, 
		    		'year' => $item->year, 
		    		'language' => $language_name, 
		    		'type' => 'course', 
		    		'platform' => str_replace('http://', "", url("/")),
		    		'platform_id' => server_property('server_id'),
		    		'published' => $item->published,
	    		];
		});

		return $data;
    }


    /**
     * All courses list
     */
    public function datatable_list(Request $request) {

    	if ($data = $this->table_data()) {
    		return Datatables::of($data)->make();	
    	} else {
    		return "{}";
    	}

    }

    /**
     * Add teacher to course
     */
    public function add_teacher(Request $request)
    {
		// Validate the request
		$this->validate(
			$request, [
				'course_id' => 'required',
				'author_id' => 'required'
				]);

		// get the models
		$user = \App\User::find($request->author_id);
    	$course = Course::find($request->course_id);

    	// attach author to course
    	if (!$course->teachers->contains($user->id)) {

    		$result = $course->teachers()->attach([$user->id]);
    		return view('courses.author', ['user' => $user]);	
    	} else { 
			return null;
    	}
    }

    
    /**
     * Remove teacher from course
     */
    public function remove_teacher(Request $request)
    {
		// Validate the request
		$this->validate(
			$request, [
				'course_id' => 'required',
				'author_id' => 'required'
				]);

	   	$course = Course::find($request->course_id);

		// attach author to course
		if ($course->teachers->contains($request->author_id)) {

			$result = $course->teachers()->detach([$request->author_id]);
			return 'true';

		} else { 
			return 'false';	
		}	
	}


	/**
	 * Remove image from course 
	 * @return OK
	 */
	public function remove_image(Request $request, Course $course)
	{
		$course->image = "";
		$course->save();
		return 'OK';
	}



	public function delete(Request $request, Course $course)
	{

		// delete the course
		$course->delete();

		// if delete from create mode
		if (isset($_GET['cancel'])) {
			flash( trans('text.course_canceled') )->important();
			// return to home 
			return redirect("/");

		} else {
			
			flash( trans('text.deleted') )->important();
			// return back
			return back();
		}
		
	}

}
