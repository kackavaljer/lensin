<?php

namespace App\Http\Controllers;

use App\StudyCase;
use App\StudyCaseGuideline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudyCaseGuidelineController extends Controller
{
    
    /**
	 * Load criteria
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function load_guidelines(Request $request)
	{
		$guidelines = StudyCaseGuideline::where('level', 0)->orderBy('level', 'order')->get();
		$data = '';
		foreach ($guidelines as $guideline) {
			$data .= view('study_cases_guidelines.edit', ['guideline' => $guideline, 'level' => 0, 'parent' => 0]);
		}
		return $data;
	}


	public function create(Request $request)
	{

		$orderGuideline = StudyCaseGuideline::where('study_case_guideline_id', $request->parrent_id)->orderBy('order', 'desc')->first();
		if (!$orderGuideline) {
			$order = 1;
		} else {
			$order = $orderGuideline->order + 1;
		}

		$guideline = StudyCaseGuideline::create([ 
			'order' => $order, 
			'study_case_guideline_id' => $request->parrent_id, 
			'level' => 	$request->level + 1, 
			'created_by' => Auth::user()->id
			]);
		return view('study_cases_guidelines.edit', ['level' => $request->level + 1, 'guideline' => $guideline]);
	}

	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		
		// Update the Course
		$guideline = StudyCaseGuideline::find($request->id);
		$guideline->$name = $request->value;
		$guideline->save();

		return 'OK';
	}


	/**
	 * DELETE STUDY CASE GUIDELINE
	 */
	public function delete(Request $request, StudyCaseGuideline $study_case_guideline)
	{
		$study_case_guideline->delete();

		flash(trans('text.study_case_deleted'))->important();
		return back();

	}

	/**
	 * DELETE STUDY CASE GUIDELINE
	 */
	public function assign(Request $request, StudyCase $study_case)
	{
		// sync the guidelines to the study case
		$study_case->guidelines()->sync($request->study_case_id);

		return "OK";
		
	}


}
