<?php

namespace App\Http\Controllers;

use App\ServerList;
use Illuminate\Http\Request;

class ServerListController extends Controller
{

	/**
     * LIST OF SERVERS
	 * @return [type] [description]
	 */
    public function index()
    {
    	$server_list = ServerList::orderBy('enabled', 'DESC')->get();
    	return view('backend.server_list', ['server_list' => $server_list]);
    }


    /**
     * CREATE A NEW SERVER
     * @return back()
     */
    public function create(Request $request)
    {
        // Validation of the request
		$this->validate($request, [
				'name' => 'required',
				'address' => 'required',
				'administrator_contact' => 'required'
				]);

        // Check if server address is this one
        if ($request->address == url('/')){
            flash( trans('text.central_server_can_not_access_itself') )->important();
            return back();
        }   

        // Create the server in the list
		$server = ServerList::create([
		        'name' => $request->name, 
		        'address' => $request->address, 
		        'enabled'  => $request->enabled, 
		        'administrator_contact' => $request->administrator_contact, 
		        'notes'  => $request->notes
			]);

        // 
		flash(trans('text.created'))->important();
		return back();
    }


    /**
     * UPDATE SERVER 
     * @return  back
     */
    public function update(Request $request, ServerList $server_list)
    {
		$this->validate($request, [
				'name' => 'required',
				'address' => 'required',
				'administrator_contact' => 'required'
			]);

		$server_list->name = $request->name;
        $server_list->address = $request->address;
        $server_list->enabled = $request->enabled ? 1 : 0;
        $server_list->administrator_contact = $request->administrator_contact;
        $server_list->server_description = $request->server_description;
        $server_list->notes = $request->notes;
        $server_list->save();

        flash(trans('text.saved'))->important();
        return back();

    }


    /**
     * LIST OF SERVERS
     * @return json
     */
    public function server_list()
    {
    	return ServerList::where('enabled', 'LIKE', 1)->get();
    }


    /**
     * DELETE SERVER FORM LIST
     * @param  ServerList $servel_list [description]
     * @return back()
     */
    public function delete(Request $request, ServerList $server_list)
    {
        // delete the server from list
        $server_list->delete();

        // flash the text to the user
        flash( trans('text.deleted') )->important();

        // return back()
        return back();
    }


}


