<?php

namespace App\Http\Controllers;

use App\Lecture;
use App\LectureContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LectureContentController extends Controller
{

	/**
	 * Load edit part for the resources
	 */
    public function load_edit(Request $request, Lecture $lecture)
    {
    	return view('courses.edit_grid_view_subject_lecture_content', ['lecture' => $lecture]);
    }

    /**
     * Create a new lecture content
     */
    public function create(Request $request, Lecture $lecture)
    {
		$lectureContent = LectureContent::create([
			'created_by' => Auth::user()->id,
			'lecture_id' => $lecture->id,
			'text' => "",
			]);

		return "OK";
    }


	/**
	 * Update from input fields
	 */
	public function update_data(Request $request)
	{	
		$name = $request->name;
		// find the Lecture
		$lectureContent = LectureContent::find($request->id);

		if (!$lectureContent) {
			return trans('text.lecture_doesnt_exist');
		}

		// Update the Lecture
		$lectureContent->$name = $request->value;
		$lectureContent->save();

		return 'OK';
	}

	/**
	 * Delete lecture content
	 */
	public function delete(Request $request, LectureContent $lectureContent)
	{
		$lectureContent->delete();
		return 'OK';
	}

}
