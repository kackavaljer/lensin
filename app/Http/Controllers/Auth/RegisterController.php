<?php

namespace App\Http\Controllers\Auth;

use Config;
use App\User;
use App\Mail\Welcome;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'user_type_id' => 'required',
            'country_id' => 'required',
            'address' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $this->redirectTo = '/';

        $user = User::create([
                'name' => $data['name'],
                'last_name' => $data['last_name'],
                'school' => $data['school'],
                'departement' => $data['departement'],
                'position' => $data['position'],
                'country_id' => $data['country_id'],
                'address' => $data['address'],
                'interest' => $data['interest'],
                'phone' => $data['phone'],
                'web' => $data['web'],
                'email' => $data['email'],
                'user_type_id' => $data['user_type_id'],
                'language' => 'en',
                'server_id' => Config::get('lens.server_id'),
                'password' => bcrypt($data['password']),
        ]);

        /**
         * Add default roles to users
         */
        $user->attachRole(role('managecourses'));
        $user->attachRole(role('managesite'));
        $user->attachRole(role('manageusers'));
        $user->attachRole(role('managestudycases'));
        $user->attachRole(role('managetools'));
        $user->attachRole(role('manageservers'));
        $user->attachRole(role('manageprojects'));
        $user->attachRole(role('managelanguages'));

        \Mail::to($user)->send(new Welcome($user));

        flash('You have registered. You will recieve an email soon.', 'success')->important();
        return $user; 
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries = \App\Country::pluck('name', 'id');
        $user_types = \App\UserType::pluck('name', 'id')->toArray();
        $user_types = array_map('name_to_text', $user_types);

        return view('auth.register', ['countries' => $countries, 'user_types' => $user_types]);
    }



    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }


}
