<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudyCaseGuideline extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'study_case_id', 'study_case_guideline_id', 'created_by', 'title', 'order', 'level', 'reference', 'published'
    ];

	
	/**
	 * Created by
	 */
	public function created_by()
	{
	    return $this->belongsTo('App\User', 'created_by');
	}


	/**
     * Study case
     */
    public function study_cases()
    {
        return $this->belongsToMany('App\StudyCase');
    }

	/**
     * Guideline parent
     */
    public function parent()
    {
        return $this->belongsTo('App\StudyCaseGuideline');
    }

	/**
     * Guideline children
     */
    public function children()
    {
        return $this->hasMany('App\StudyCaseGuideline');
    }


}
