<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resource extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'name', 'category', 'desctiption', 'order', 'filename', 'link', 'published', 'resourcable_id', 'resourcable_type'
    ];


	/**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

	


    /**
     * Get all of the owning commentable models.
     */
    public function resourcable()
    {
        return $this->morphTo();
    }    


    /**
     * Licences
     */
    public function licences()
    {
        return $this->belongsToMany('App\Licence');
    }


    /**
     * Resource types
     */
    public function resource_type()
    {
        return $this->belongsTo('App\ResourceType');
    }    


}


