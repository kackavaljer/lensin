<?php

namespace App;

use HttpOz\Roles\Traits\HasRole;
use HttpOz\Roles\Contracts\HasRole as HasRoleContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements HasRoleContract
{
    use Notifiable, HasRole;
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_id', 'language', 'country_id', 'email', 'password', 'name', 'last_name', 'phone', 'web', 'user_type_id', 'school', 'address', 'departement', 'position', 'interest', 'gps', 'facebook_id' , 'twitter_id', 'google_id', 'linked_id', 'validated', 'newsletter', 'public_info', 'note', 'birthday',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Types of the user
     */
    public function types()
    {
        return $this->belongsTo('App\UserType');
    }    

    /**
     * Courses created
     */
    public function courses_created()
    {
        return $this->hasMany('App\Course', 'created_by');
    }


    /**
     * Teachers 
     */
    public function courses_teached()
    {
        return $this->belongsToMany('App\Course', 'course_teacher');
    }
    

    public function hasGroup($group)
    {
        return ($this->getRoles()->where('group', 'LIKE', $group)->first() ? true : false);
    }

    public function groups()
    {
        return ($this->getRoles());   
    }

    /**
     * Description
    */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }    

}
