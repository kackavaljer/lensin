<?php
	$parameters = ['name', 'last_name', 'email', 'address', 'city', 'region', 'website', 'phone', 'fax', 'description'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Seeon - User profile changed</title>
</head>
<body>

	Hello,<br>

	<h2>User profile changed</h2>
	The user <b>{{ $user->company_name }} </b> has changed his profile... here are the changes:
	<table>
		<tr>
			<td></td>
			<td>OLD</td>
			<td>NEW</td>
		</tr>
		@foreach ($parameters as $parameter)
			@if ($user->$parameter != $old_user->$parameter)
				<tr>
					<td><small>{{ $parameter }}<small></td>
					<td>{{ $old_user->$parameter }}</td>
					<td><strong>{{ $user->$parameter }}</strong></td>
				</tr>
			@endif
		@endforeach
	</table>
	<br><br>

</body>
</html>
