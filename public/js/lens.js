
$(document).ready(function(){


    // MAKE TOOLTIPS
    $( document ).tooltip();

    // UPDATE INPUT FIELD
    $('body').on('change', '.update_input', function(){

        var object = $(this);
        var value = $(this).val();
        var update_url = '/' + $(this).data('model') + '/update_data';
        var id = $(this).data('id');
        var name = $(this).attr('name');
        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                'id': id,
                'value': value,
                'name': name,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(response) {
                if (response == "OK") {
                    object.animate({
                        backgroundColor: "#90EE90"
                    }, 300);
                    object.animate({
                        backgroundColor: "white"
                    }, 400).delay(400);

                } else {
                    //bootbox.alert (response);
                }
                
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
    });


    // UPDATE CHECKBOX
    $('body').on('change', '.update_checkbox', function(){

        var object = $(this);
        var id = $(this).data('id');
        var id_belongs_to = $(this).data('id-belongs-to');
        var update_url = $(this).data('url');
        var token = $("meta[name='csrf-token']").attr("content"); 

        if ($(this).is(":checked")) {
            var value = 1;
        } else {
            var value = 0;
        }
        
        $.ajax({
            data: { 
                'id': id,
                'value': value,
                'id_belongs_to': id_belongs_to,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(response) {
                //alert(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
    });


    // DELETE RESOURCE 
    $('body').on('click', '.delete-resource', function(){

        var update_url = '/' + $(this).data('model') + '/delete';
        var id = $(this).data('id');
        var token = $("meta[name='csrf-token']").attr("content"); 
        var delete_id =  $(this).data('delete-id');
        var text =  $(this).data('text');
        
        bootbox.confirm(text, function(result){ 
            // if OK, delete the model
            if (result) {
                $.ajax({
                    data: { 
                        'id': id,
                        '_token': token,
                    },
                    url: update_url,
                    type: 'POST',

                    success: function(html) {
                        // remove the div 
                        $("#" + delete_id).remove();
                    },

                    error: function(response) {
                        bootbox.alert("err" + response);  
                    }

                });
            } 
        });

    });


    // BUTTON AJAX
    $('body').on('click', '.ajax_btn', function(){

        var update_url = $(this).data('url');
        var id = $(this).data('id');
        var data = $(this).data('data');
        var token = $("meta[name='csrf-token']").attr("content"); 
        var return_fn =  $(this).data('return_fn');
        var confirm_text =  $(this).data('confirm_text');

        if (confirm_text) {

            bootbox.confirm(confirm_text, function(result){ 
                // if OK, delete the model
                if (result) {

                    $.ajax({
                        data: { 
                            'id': id,
                            'data': data,
                            '_token': token,
                        },
                        url: update_url,
                        type: 'POST',

                        success: function(return_data) {
                            // if there is a return function execute it with return data
                            if (return_fn) {
                                window[return_fn](return_data);
                            }
                        },

                        error: function(response) {
                            bootbox.alert("err" + response);  
                        }

                    });
                }
            }); 
        }
    });
        




   // PUBLISH RESOURCE 
    $('body').on('click', '.publish_resource', function(){

        var update_url = $(this).data('url');
        var id = $(this).data('id');
        var token = $("meta[name='csrf-token']").attr("content"); 
        var object = $(this);

        $.ajax({
            data: { 
                'id': id,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(html) {
                // update the icon
                object.html(html)
            },
            error: function() {
                bootbox.alert('Error');  
            }
        });
    });


    // OPEN MODAL AFTER URL
    $('body').on('click', '.modal_url', function(){

        var update_url = $(this).data('url');
        var token = $("meta[name='csrf-token']").attr("content"); 
        $.ajax({
            data: { 
                '_token': token,
            },
            url: update_url,
            type: 'POST',
            success: function(html) {
                
                // reset the function on close
                $('body').on('hidden.bs.modal', '#main_modal', function(){});
                
                // place responce in the modal
                $("#main_modal").html(html)
                
                // open the modal
                $('#main_modal').modal('show'); 
            },

            error: function() {
                bootbox.alert('Error');  
            }
            
        });

    });




});