<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by')->unsigned();
            $table->integer('resourcable_id')->unsigned();
            $table->string('resourcable_type')->nullable();
            $table->string('author')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('published')->nullable();
            $table->integer('order')->nullable();
            $table->string('filename')->nullable();
            $table->integer('resource_type_id')->nullable();
            $table->string('year')->nullable();
            $table->string('licence')->nullable();
            $table->string('link')->nullable();
            $table->string('length')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
