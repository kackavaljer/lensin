<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });

        // insert default licences
        DB::table('licences')->insert([
            ["name" => "Attribution", "description" => "Licensees may copy, distribute, display and perform the work and make derivative works and remixes based on it only if they give the author or licensor the credits (attribution) in the manner specified by these."], 
            ["name" => "Share-alike", "description" => "Licensees may distribute derivative works only under a license identical ('not more restrictive') to the license that governs the original work. (See also copyleft.) Without share-alike, derivative works might be sublicensed with compatible but more restrictive license clauses, e.g. CC BY to CC BY-NC.)"], 
            ["name" => "Non-commercial", "description" => "Non-commercial (NC) Licensees may copy, distribute, display, and perform the work and make derivative works and remixes based on it only for non-commercial purposes."], 
            ["name" => "Non-derivative", "description" => "No Derivative Works (ND)"], 
        ]);  
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licences');
    }
}
