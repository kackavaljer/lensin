<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ['locale' => "en", 'group' => "languages", 'item' => "group", 'text' => "Group"],
            ['locale' => "en", 'group' => "languages", 'item' => "name", 'text' => "Name"],
            ['locale' => "en", 'group' => "languages", 'item' => "text", 'text' => "Text"],
            ['locale' => "en", 'group' => "languages", 'item' => "locale", 'text' => "Locale"],
            ['locale' => "en", 'group' => "text", 'item' => "manage", 'text' => "Manage"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
