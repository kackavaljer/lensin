<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ['locale' => "en", 'group' => "languages", 'item' => "language_list", 'text' => "Languages list"],
            ['locale' => "en", 'group' => "languages", 'item' => "add_new_language", 'text' => "Add new language"]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

