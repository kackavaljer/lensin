<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ['locale' => "en", 'group' => "backend", 'item' => "platform_properties", 'text' => "Platform properites"],
            ['locale' => "en", 'group' => "backend", 'item' => "you_need_to_register_your_platform_on_central_server", 'text' => "You need to register your platform on central Lens server."],
            ['locale' => "en", 'group' => "backend", 'item' => "register_platform_on_central_server", 'text' => "Register this platform on central Lens server."],
        ]);    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
