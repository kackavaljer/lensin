<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ['locale' => "en", 'group' => "backend", 'item' => "server_connected_translations_pulled", 'text' => "Platform connected to Lens central server. Languages pulled."],
            ['locale' => "en", 'group' => "backend", 'item' => "central_server_properties", 'text' => "Central server connection properties"]
        ]);    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
