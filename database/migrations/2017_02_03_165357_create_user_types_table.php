<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        // Insert default user types
        DB::table('user_types')->insert([
            ['name' => 'student'],
            ['name' => 'teacher'],
            ['name' => 'school'],
            ['name' => 'designer_design_company'],
            ['name' => 'company'],
            ['name' => 'ngo'],
            ['name' => 'goverment_institution'],
            ['name' => 'others']
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');
    }
}
