<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ['locale' => "en", 'group' => "backend", 'item' => "distribute_translations_to_local_platforms", 'text' => "Distribute translations to local platforms"],
            ['locale' => "en", 'group' => "text", 'item' => "please_wait", 'text' => "Please wait..."]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
