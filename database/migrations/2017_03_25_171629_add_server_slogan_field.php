<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServerSloganField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('server_properties', function (Blueprint $table) {
            $table->text('data')->nullable()->change();
        });        
        
        DB::table('server_properties')->insert(
          ['name' => 'slogan', 'data' => ' ']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
