<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // insert default licences
        DB::table('roles')->insert([
            ["name" => "ManageCourses", "slug" => 'managecourses', "group" => "admin"], 
            ["name" => "ManageSite", "slug" => 'managesite', "group" => "admin"], 
            ["name" => "ManageUsers", "slug" => 'manageusers', "group" => "admin"], 
            ["name" => "ManageStudyCases", "slug" => 'managestudycases', "group" => "admin"], 
            ["name" => "ManageServers", "slug" => 'manageservers', "group" => "admin"], 
            ["name" => "ManageTools", "slug" => 'managetools', "group" => "admin"], 
            ["name" => "ManageProjects", "slug" => 'manageprojects', "group" => "admin"], 
            ["name" => "ManageLanguages", "slug" => 'managelanguages', "group" => "admin"], 
        ]);       
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
