<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Texts for the frontend
    |--------------------------------------------------------------------------
    |
    */

// COMMON
    'confirm' => 'Confirm',
    'new' => 'New',
    'create' => 'Create',
    'created' => 'Created',
    'modify' => 'Modify',
    'delete' => 'Delete',
    'deleted' => 'Deleted',
    'back' => 'Back',
    'cancel' => 'Cancel',
    'info' => 'Info',
    'syllabus' => 'Syllabus',
    'insert_title' => 'Insert title',
    'cover_image' => 'Cover image',
    'resource_title' => 'Resource title',
    'type' => 'Type',
    'notes' => 'Notes',
    'title' => 'Title',
    'close' => 'Close',
    'save' => 'Save',
    'saved' => 'Saved',
    'enabled' => 'Enabled',
    'description' => 'Description',
    'insert_description' => 'Insert Description',
    'publish' => 'Publish',
    'unpublish' => 'Unpublish',
    'published' => 'Published',
    'unpublished' => 'Unpublished',
    'contents' => 'Contents',
    'select_cover_image' => 'Select cover image',
    'author' => 'Author',
    'length' => 'Length',
    'drop_or_click_to_select_image' => 'Drop or click to select image.',
    'uploading' => 'Uploading...',
    'upload_error' => 'There was an error during upload... Try again or contact the administrator. ',
    'licence' => 'Licence',
    'file_type' => 'File type',
    'website' => 'Website',
    'no_preview_available' => 'No preview avaliable.',
    'not_authorised' => 'You are not authorised to view this page.',
    'click_to_select_file' => 'Click here to upload a file.',
    'create_mode' => 'Create mode',
    'modify_mode' => 'Modify mode',
    'register_success_text' => 'You have registered. You will recieve an email soon.',
    'logout_text' => 'You have logged out',
    

// BACKEND 
    'backend' => 'Backend',
    'welcome_to_backend' => 'Welcome to backend',
    'site' => 'Site',
    'server_logo' => 'Server logo',
    'server_round_logo' => 'Server logo ON',
    'server_round_transparent_logo' => 'Server logo OFF',
    'central_server_can_not_access_itself' => 'Central server can not access itself!',
    'logo_updated' => 'Server logo updated',
    'slogan' => 'Tagline (text under the logo)',
    'platform_title' => 'Platform name',

// HEADER
    'learn_about' => 'Learn about...',
    'advanced_research' => 'ADVANCED RESEARCH',

// HEADER MENU
    'about' => 'About',
    'labs' => 'Labs',
    'network' => 'Network',
    'tutorial' => 'Tutorial',
    'contact' => 'Contact',
    'types' => 'Types',
    'authors' => 'Authors',
    'year' => 'Year',
    'confrm' => 'Confrm',

// USER TYPES
    'student' => 'Student',
    'teacher' => 'Teacher',
    'teachers' => 'Teachers',
    'school' => 'School',
    'designer_design_company' => 'Designer/design company',
    'company' => 'Company',
    'ngo' => 'NGO',
    'goverment_institution' => 'Goverment institution',
    'others' => 'Others',

// USER FIELDS
    'language' => 'Language',
    'country' => 'Country',
    'email' => 'E-mail',
    'password' => 'Password',
    'name' => 'Name', 
    'last_name' => 'Last name', 
    'insert_last_name' => 'Insert last name', 
    'phone' => 'Phone', 
    'insert_phone' => 'Insert phone', 
    'web' => 'Website', 
    'insert_web' => 'Insert website', 
    'user_type'  => 'User type', 
    'school' => 'School', 
    'insert_school' => 'Insert school', 
    'univeristy_institution_company' => 'Univeristy/Institution/Company', 
    'address' => 'Address', 
    'insert_address' => 'Insert address', 
    'departement' => 'Departement', 
    'insert_departement' => 'Insert departement', 
    'position' => 'Position', 
    'insert_position' => 'Insert position', 
    'interest' => 'Interest', 
    'insert_interest' => 'Insert interest', 
    'gps' => 'GPS', 
    'facebook' => 'Facebook', 
    'twitter' => 'Twitter', 
    'google' => 'Google', 
    'linked_in' => 'LinkedIn', 
    'newsletter' => 'Newsletter', 
    'public' => 'Public', 
    'note' => 'Note', 
    'birthday' => 'Birthday', 
    'user_profile' => 'User profile', 
    'profile_image' => 'Profile image', 
    'image_updated' => 'Profile image updated', 

// USER
    'general_information' => 'General information',
    'user_priviledges' => 'User priviledges',
    'user' => 'User',
    'users' => 'Users',
    'login' => 'Login',
    'email_address' => 'Email Address',
    'remember_me' => 'Remember me',
    'forgot_password' => 'Forgot Your Password?',
    'join_as_participant' => 'Join as participant',
    'not_registered' => 'Not registered',
    'register' => 'Register',
    'confirm_password' => 'Confirm Password',
    'select_country' => 'Please select country',
    'research_interest' => 'Teaching and research interest',
    'profile' => 'Profile',
    'logout' => 'Logout',
    'platform' => 'Platform',
    'insert_name' => 'Insert name',

// USER PRIVILEDGES
    'managecourses' => 'Manage courses',
    'managesite' => 'Manage Lens website',
    'manageusers' => 'Manage users',
    'managestudycases' => 'Manage study cases',
    'manageservers' => 'Manage servers',

// LEFT MENU
    'resources' => 'Resources',
    'courses' => 'Courses',
    'course' => 'Course',
    'lectures' => 'Lectures',
    'lecture' => 'Lecture',
    'tools' => 'Tools',
    'cases' => 'Cases',
    'projects' => 'Projects',


// COURSES
    'courseware' => 'Courseware',
    'course_created' => 'Course created.',
    'edit_course' => 'Edit course',
    'question_delete_course' => 'Are you sure you want to delete this course and all its lectures?',
    'course_canceled' => 'Create canceled',
    'tooltip_delete_course' => 'Delete this course',
    'tooltip_unpublish_course' => 'This course is published. Click here to unpublish it.',
    'tooltip_publish_course' => 'This course is not published. Click here to publish it.',


// COURSE SUBJECT
    'insert_subject_title' => 'Insert subject text',
    'add_lecture' => 'Add lecture',
    'add_subject' => 'Add subject',
    'course_subject_deleted' => 'Course subject deleted.',
    'course_subject' => 'Course subject',
    'course_subject_doesnt_exist' => 'This course subject doesnt exist. ',
    'question_delete_course_subject' => 'Are you sure you want to delete this subject and all its lectures?',
    'no_subjects' => 'There are no published subjects for this course.',

// LECTURES 
    'insert_lecture_title' => 'Insert lecture title',
    'insert_lecture_contents' => 'Insert lecture contents',
    'question_delete_lecture' => 'Are you sure you want to delete this lecture?',
    'lecture_doesnt_exist' => 'This lecture doesnt exist. ',
    'lecture_deleted' => 'Lecture deleted.',
    'no_lectures' => 'There are no published lectures for this subject.',

// RESOURCES 
    'add_new_resource' => 'Add new resource',
    'question_delete_resource' => 'Are you sure you want to delete this resource?',
    'resource_deleted' => 'Resource deleted.',
    'resource_does_not_exist' => 'Resource does not exist.',
    'add_resource_information' => 'Add resource information',
    'upload_resource_file' => 'Upload resource file',
    'file' => 'File',
    'delete_file' => 'Delete file',
    'no_resources' => 'There are no published resources for this lecture.',
    'single_file' => 'Single file',
    'multiple_files' => 'Multiple files',
    'what_type_of_file_are_you_uploading' => 'What type of files are you uploading?',

// STUDY CASES
    'study_cases' => 'Study cases',
    'study_case' => 'Study case',
    'studies' => 'Studies',
    'criteria' => 'Criteria',
    'criteria_and_guidelines' => 'Criteria and guidelines',
    'location' => 'Location',
    'state' => 'State',
    'study_case_title' => 'Study case title',
    'category' => 'Category',
    'producer' => 'Producer',
    'designer' => 'Designer',
    'sustainable_benefits' => 'Sustainable benefits',
    'study_case_created' => 'Study case created!',
    'study_case_deleted' => 'Study case deleted!',
    'delete_study_case' => 'Delete',
    'delete_study_case_title' => 'Delete study case?',
    'study_case_delete_text' => 'These items will be permanently deleted and cannot be recovered. Are you sure?',
    'dimension' => 'Dimension',
    'criteria' => 'Criteria',
    'guideline' => 'Guideline',
    'guidelines' => 'Guidelines',
    'subguideline' => 'Subguideline',
    'subguidelines' => 'Subguidelines',
    'report' => 'Report',
    'upload_report' => 'Upload report',
    'delete_report' => 'Delete report',
    'delete_report_confirmation' => 'Are you sure you want to delete report?',
    'download_current_report' => 'Download current report',


// TOOLS
    'tools' => 'Tools',
    'tool' => 'Tool',
    'question_delete_tool' => 'Are you sure you want to delete this tool?',
    'insert_category' => 'Insert category',
    'managetools' => 'Manage tools',

    // tooltips 
    'tooltip_modify_tool' => 'Modify this tool',
    'tooltip_delete_tool' => 'Delete this tool',
    'tooltip_unpublish_tool' => 'This tool is published. Click here to unpublish it.',
    'tooltip_publish_tool' => 'This tool is not published. Click here to publish it.',


/*** PR0JECTS ***/
    'manageprojects' => 'Manage projects',

// THEMES
    'project_theme' => 'Project theme',
    'theme' => 'Theme',
    'themes' => 'Themes',
    'theme_name' => 'Theme name',
    'edit_theme' => 'Edit theme',
    'create_theme' => 'Create theme',
    'select_theme' => 'Select theme',
    'tooltip_modify_theme' => 'Modify theme',
    'tooltip_unpublish_theme' => 'Unpublish theme',
    'tooltip_publish_theme' => 'Publish theme',
    'create_new_theme' => 'Create a new theme',
    'tooltip_delete_theme' => 'Delete theme',
    'question_delete_theme' => 'Are you sure you want to delete this theme? THIS WILL ERASE ALL CHALANGES AND PROJECTS IN THIS THEME!',


// CHALLENGES
    'challenges' => 'Challenges',
    'create_challenge' => 'Create challenge',
    'edit_challenge' => 'Edit challenge',
    'tooltip_modify_challenge' => 'Modify challenge',
    'tooltip_unpublish_challenge' => 'Unpublish challenge',
    'tooltip_publish_challenge' => 'Publish challenge',
    'tooltip_delete_challenge' => 'Delete challenge',
    'question_delete_challenge' => 'Are you sure you want to delete this challenge? THIS WILL ERASE ALL PROJECTS IN THIS CHALLENGE!',
    'insert_course_name' => 'Insert course name',
    'enable_users_comments' => 'Enable user comments',
    'comments' => 'Comments',


// SERVER
    'server_id' => 'Server ID (get from the central server)',
    'server_list' => 'Server list',
    'server_id_updated' => 'Server ID updated!',
    'footer_social_icons' => 'Footer seocial icons',
    'central_server_address' => 'Central server URL',
    'add_new_server' => 'Add a new server',
    'servers' => 'Server list',
    'no_server_found' => 'No server found!',
    'administrator_contact' => 'Administrator contact',

// TOOLTIPS 
    'tooltip_server_logo' => 'Logo that appears on this server in the top left corner.',
    'tooltip_server_round_logo' => 'Logo that appears on resources, platform list etc.',
    'tooltip_server_round_logo_transparent' => 'Transparent logo that appears on resources, platform list etc. when the switch is off.',


];
