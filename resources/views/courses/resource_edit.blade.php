<div class="row resource-div hover" id="resource_row_{{ $resource->id }}">
	
	<!-- resource file type -->
	<div class="col-xs-1" id="resource_{{ $resource->id }}_file_type" style="font-size: 14pt; padding-top: 4px;">
		@if ($resource->resource_type)
			{!! $resource->resource_type->icon !!}
		@endif
	</div>
	
	<!-- resource name -->
	<div class="col-xs-4 top-padding-small" id="resource_{{ $resource->id }}_name" >
		{{ $resource->name }}
	</div>
	
	<!-- resource author -->
	<div class="col-xs-3 top-padding-small" id="resource_{{ $resource->id }}_author">
		{{ $resource->author }}	
	</div>
	
	<!-- resource year -->
	<div class="col-xs-1 top-padding-small"  id="resource_{{ $resource->id }}_year" >
		{{ $resource->year }}
	</div>

	<!-- resource manage buttons -->
	<div class="col-xs-3 text-right">
		<!-- edit resource -->
		<div class="btn bck-course edit_resource" data-id="{{ $resource->id }}" ><i class="fa fa-pencil" aria-hidden="true"></i></div>
		<!-- delete resource -->
		<button data-model='resources' data-delete-id="resource_row_{{ $resource->id }}" data-id={{ $resource->id }} data-text='{{ trans("text.question_delete_resource") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
		<!-- publish / unpublish resource -->
		<?php $icon = ($resource->published == 1) ? "fa fa-eye" : 'fa fa-eye-slash'; ?>
		<div class="btn bck-course publish_resource" data-url="/resources/publish" data-id="{!! $resource->id !!}" data-delete-id="resource_row_{!! $resource->id !!}">
			<i class="{!! $icon !!}" aria-hidden="true"></i>
		</div>		
	</div>

</div>