<!-- FILE LIST -->
<?php 
	$resource_files = json_decode($resource->path);
?>

@if( $resource_files )
	@foreach ($resource_files as $resource_file)
		
		<div class="row" style="padding-top: 5px; padding-bottom:5px; border-bottom: 1px dashed #CCC;">
			<div class="col-xs-10" style="padding-top: 10px;">
				{!! $resource_file !!}
			</div>
			<div class="col-xs-2">
				<div class="round_button btn-danger delete_resource_file" data-resource-id="{!! $resource->id !!}" data-file-index="{!! $loop->index !!}"><i class="fa fa-times" aria-hidden="true"></i></div>
			</div>
		</div>
		
	@endforeach
@endif