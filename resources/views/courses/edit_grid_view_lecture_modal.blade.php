<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('text.lecture') }}</h4>
		</div>
		<div class="modal-body">

			<!-- lecture name-->
			<div class="row padding-small  hover">
				<div class="col-xs-3">
					{{ trans('text.title') }}
				</div>
				<div class="col-xs-9">
					<input id="lecture_name_modal" type="text" name="name" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->name }}" placeholder="{{ strtoupper(trans('text.insert_lecture_title')) }}" class="form-control update_input">
				</div>
			</div>

			<!-- lecture description -->
			<div class="row padding-small  hover">
				<div class="col-xs-3">
					{{ trans('text.contents') }}
				</div>
				<div class="col-xs-9">
					<input type="text" name="description" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->description }}" placeholder="{{ strtoupper(trans('text.insert_lecture_contents')) }}" class="form-control update_input">
				</div>
			</div>		

			<!-- cover image -->
			<div class="row padding-small hover">
				<div class="col-xs-3">
					{{ trans('text.cover_image') }}
				</div>
				<div class="col-xs-5">
					<div class="dropzeon dropzone_div" id="lecture_upload_image_{{ $lecture->id }}" class="image" >
						{{ trans('text.click_to_select_file') }}
					</div>
				</div>
				<div class="col-xs-4 text-right" id="lecture_image_{{ $lecture->id }}">
					@if ($lecture->image)
						<img src="{{ $lecture->image }}" style="max-height: 150px; max-width: 150px;">
					@endif
				</div>				
			</div>	

			<!-- resources -->
			<div class="row  padding-small ">
				<div class="col-xs-3">
					{{ trans('text.resources') }}
				</div>
				<div class="col-xs-9">
					<div id="lecture_{{ $lecture->id }}_resources" style="width: 100%;">
						@if ($lecture->resources)
							@foreach ($lecture->resources as $resource)
								@include('courses.resource_edit', ['resource' => $resource])
							@endforeach
						@endif
					</div>
					<div class="clearfix"></div>

					<!-- DROPZONE -->
					<div class="dropzeon dropzone_div" id="resource_upload_{{ $lecture->id }}">{{ trans('text.click_to_select_file') }}</div>

				</div>
			</div>		
		</div>
	</div>

</div>


<script type="text/javascript">

	$("#lecture_upload_image_{{ $lecture->id }}").dropzone({
		url: "/lectures/upload_cover_image/{{ $lecture->id }}",
		paramName: "image",
		previewsContainer: false,
		uploadMultiple: false,
		parallelUploads: 1,		
		
		sending: function(file, xhr, formData) {
			formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
		},

		uploadprogress: function(file, progress, bytesSent) {
			// upload progress percentage
		    $("#lecture_upload_image_{{ $lecture->id }}").html(parseInt(progress) + "%");
		},

		success: function(file, responce) {

			// change image
			if (responce != "upload_error") {
				$('#lecture_upload_image_{{ $lecture->id }}').html('{{ trans('text.click_to_select_file') }}');
				$('#lecture_image_{{ $lecture->id }}').html('<img src=' + responce +' style="max-height: 150px; max-width: 150px;">');

			} else {
				alert ("{{ trans('text.upload_error') }}");
			}
		}
	});


	// DROPZONE - upload resouce and open resource modal
	$("div#resource_upload_{{ $lecture->id }}").dropzone({ 
		url: "/resources/create_and_upload_files/{{ $lecture->id }}",
		paramName: "resource_file",
		previewsContainer: false,
		uploadMultiple: true,
		parallelUploads: 30,

		sending: function(file, xhr, formData) {
			formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
		},

		uploadprogress: function(file, progress, bytesSent) {
			// upload progress percentage
		    $("#resource_upload_{{ $lecture->id }}").html(parseInt(progress) + "%");
		},

		successmultiple: function(file, responce) {
			console.log('done');
			// id of the parrent lecture
			var lecture_id = {{ $lecture->id }};

			//add the row and open the modal
			if (responce != "upload_error") {
				
				$('#lecture_' + lecture_id + "_resources").append(responce[1]);
				edit_resource(responce[0]);

			} else {

				alert ("{{ trans('text.upload_error') }}");
			}

			// Return text to "click here..."
			$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.click_to_select_file') }}");
			
		},

		// error return function
		error: function (file, responce){
			$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
		},

		init: function (){
			
			myDropzone = this;
		},

	});


	// On modal close 
	$('body').on('hidden.bs.modal', '#main_modal', function(){
	
		var token = $("meta[name='csrf-token']").attr("content"); 
	
		$.post({
				data: {
				'_token': token,
		      },
			url: '/lectures/grid_view_edit/{{ $lecture->id }}',
			success: function(data) {
				$('[data-lecture-id="{{ $lecture->id }}"]').replaceWith(data);
			}
		})
		
	});

</script>

