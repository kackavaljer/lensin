@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<!-- MENU LEFT -->
			<div class="col-xs-2">
				@include('left_menu_filter', ['active_switch' => "courses"])
			</div>

			<!-- CONTENT -->
			<div class="col-xs-10">

                <!-- content header -->
                <div class="row content-header">
                    
                    <!-- left button -->
                    <div class="col-xs-2">
                    </div>
                    
                    <!-- middle -->
                    <div class="col-xs-8 text-center">
						<ul id="content_tabs">
						  <li class="btn nav-course active"><a data-toggle="tab" href="#course_info">{{ strtoupper(trans('text.info')) }}</a></li>
						  <li class="btn nav-course"><a data-toggle="tab" href="#course_courseware">{{ strtoupper(trans('text.courseware')) }}</a></li>
						</ul>
						<script>
							$('#content_tabs a').click(function (e) {
							  	e.preventDefault()
							  	$(this).tab('show')
							})
						</script>
					</div>
                    
                    <!-- right -->
                    <div class="col-xs-2">
                        <!-- manage button -->
                        @include('home_manage_button')
                    </div>

                </div>


				<div class="row tab-content">
					<!-- COURSE INFO -->
					<div role="tabpanel" class="tab-pane active" id="course_info">
						<!-- title -->
		                <div class="row" style="margin-bottom: 10px;">
		                	<div class="col-xs-12">
		                		<img src="/images/resource/course.png" class="table_view_image"> <span class="pill bck-course">{{ $course['name'] }}</span>
		                	</div>
		                </div>

		                <!-- info -->
		                <div class="row">
		                	<div class="col-xs-4">
		                		<!-- image --> 
		                		@if (isset($course['image']))
		                			<span class="title">
		                				{!! strtoupper(trans('text.cover_image')) !!}
		                			</span>
		                			<img src='{!! $course["image"] !!}' class="image" style="margin-bottom: 10px;">
		                		@endif


		                		<!-- info -->
		                		<table style="width:100%;">
		                			<tr>
		                				<td>
		                					<!-- school -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.school')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
											{{ echo_index($course, 'school') }}
		                				</td>
		                			</tr>
		                			<tr>
		                				<td>
		                					<!-- year -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.year')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ echo_index($course, 'year') }}
		                				</td>
									</tr>
									<tr>
		                				<td>
		                					<!-- country -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.country')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ isset($course['country']['name']) ? $course['country']['name'] : "" }}
		                				</td>
		                			</tr>
									<tr>
		                				<td>
		                					<!-- type -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.type')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ echo_index($course, 'type') }}
		                				</td>
		                			</tr>
		                			<tr>
		                				<td>
		                					<!-- language -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.language')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ isset($course['language']['name']) ? $course['language']['name'] : "" }}
		                				</td>
		                			</tr>
		                		</table>	                		
		                	</div>

		                	<div class="col-xs-6">
		                		<!-- syllabus -->
		                		<div class="title">
		                			{{ strtoupper(trans('text.syllabus')) }}
		                		</div>
		                		{{ echo_index($course, 'description') }}
		                	</div>
		                	<div class="col-xs-2">
		                		<!-- teachers -->
		                		<div class="title">
		                			{{ strtoupper(trans('text.authors')) }}
		                		</div>
		                		@if(isset($course['teachers']))
									@foreach($course['teachers'] as $teacher)
										<div class="text-course title remove_teacher">
											<img src="{!! user_image($teacher['id']) !!} " class="image">
											{{ echo_index($teacher, 'name') }} {{ echo_index($teacher, 'last_name') }}
										</div>
									@endforeach
								@endif
		                	</div>
		                </div>
					</div>

					<!-- courseware -->
					<div role="tabpanel" class="tab-pane" id="course_courseware">
						@include('courses.view_courseware', ['course' => $course])
					</div>
					
				</div>

			</div>
		</div>
	</div>

@endsection