<li data-subject-id='{!! $courseSubject->id !!}' class="sortable_li" id="course_subject_{{ $courseSubject->id }}" style="margin-bottom: 20px;">
	<div class="row">

		<!-- LEFT SIDE -->
		<div class="col-xs-1"  style="padding: 10px; ">
			<!-- delete button -->
			<button data-model='course_subjects' data-delete-id="course_subject_{{ $courseSubject->id }}" data-id={{ $courseSubject->id }} data-text='{{ trans("text.question_delete_course_subject") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
			<br>

			<!-- publish button -->
			<?php $icon = ($courseSubject->published == 1) ? "fa fa-eye" : 'fa fa-eye-slash'; ?>
			<div class="btn bck-course publish_resource" data-url="/course_subjects/publish" data-id="{!! $courseSubject->id !!}" style="margin-top: 10px;">
				<i class="{!! $icon !!}" aria-hidden="true"></i>
			</div>
		</div>

		<!-- RIGHT SIDE -->
		<div class="col-xs-11" style="background-color: #f2f2f2; padding: 10px; padding-bottom: 10px;" id="course_subject_{{ $courseSubject->id }}_collapse">
			<!-- collapse header-->
			<div>
				<!-- course subject name-->
				<input  style="width: 90%;" type="text" name="name" data-model='course_subjects' data-id={{ $courseSubject->id }} value="{{ $courseSubject->name }}" placeholder="{{ strtoupper(trans('text.insert_subject_title')) }}" class="form-control update_input">
			</div>

			<!-- collapse content -->
			<span class="collapsable_arrow"><i class="fa fa-sort-asc" aria-hidden="true"></i></span>

			<div class="row">
				<ul id="sortable_lectures_{!! $courseSubject->id !!}" class="sortable_ul">
					@foreach ($courseSubject->lectures as $lecture)
						@include ('courses.edit_grid_view_subject_lecture', ['lecture' => $lecture])
					@endforeach

					<li class="col-xs-3 lecture_grid_li">
						<!-- lecture image -->
						<div class="lecture_grid_image_add">
							<img src="/images/resource/lecture_big.png">
							<br>
							<br>							
							<button type="button" class="btn bck-lecture shadow add_lecture_grid" data-course-subject-id="{!! $courseSubject->id !!}"><i class="fa fa-plus" aria-hidden="true"></i></button>
							<br>
						 	<span class="text-lecture">{{ strtoupper(trans('text.add_lecture')) }}</span>
						</div>
					</li>

				</ul>
			</div>

			<!-- sorting handle -->
			<span class="movable_arrow"><i class="fa fa-arrows-v" aria-hidden="true"></i></span>

		</div>
	</div>

	<script type="text/javascript">

		function sort_course_subject_{!! $courseSubject->id !!}() {
			$( "#course_subject_{!! $courseSubject->id !!}_collapse" ).accordion({
		    	header: "> span",
		    	handle: ".movable_arrow",
		    	collapsible: true,
		    	heightStyle: "content" 
		  	})

			$( "#sortable_lectures_{!! $courseSubject->id !!}" ).sortable({
				stop: function( event, ui ) {
					// get the token
					var token = $("meta[name='csrf-token']").attr("content"); 

					// make subject id-s		
					var lectures = [];

					$(this).children('li').each(function(i){
						console.log($(this).data('lecture-id'))
						//alert ($(this).data('lecture-id'));
						lectures.push($(this).data('lecture-id'));
					});

					// send to server
					$.post({
						url: '/lectures/update_sort',
						data: {
							'_token': token,
							'lectures': lectures
						},
						success: function(data, status) {
							//alert(data);
						},
						error: function(xhr, desc, err) {
							console.log(xhr);
							console.log("Details: " + desc + "\nError:" + err);
						}
					});
			  	}
			});
		};

		sort_course_subject_{!! $courseSubject->id !!}();		
	</script>

</li>


