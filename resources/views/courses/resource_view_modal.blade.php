<?php 
	$resources_files = json_decode($resource->path);
?>
<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content" style="font-size: 13pt;">
		<div class="modal-body">

			<!-- header -->
			<div class="row" style="margin-bottom: 20px;">
				<!-- resource icon -->
				<div class="col-xs-1 col-sm-offset-1 text-resource">
					{!! ($resource->resource_type) ? $resource->resource_type->icon : "" !!}
				</div>
				<!-- resource name -->
				<div class="col-xs-8 text-center resource_text text-lecture">
					<div class="round_button resouce-button bck-resource" style="display: inline-block;">&nbsp;</div> {{ $resource->resourcable->name }}
				</div>
				<!-- close button-->
				<div class="col-xs-2 text-right">
					<button type="button" class="btn round_button text-course" style="background-color: #f2f2f2" data-dismiss="modal">X</button>
				</div>
			</div>

			<!-- content -->
			<div class="row">
				<!-- left -->
				<div class="col-xs-1">
					
				</div>
				<!-- content -->
				<div class="col-xs-4">
					<table style="width: 100%">
						<!-- name -->
						<tr>
							<td class="text-lecture">{{ trans('text.name') }}</td>
							<td>{{ $resource->name }}</td>
						</tr>
						<!-- author -->
						<tr>
							<td class="text-lecture">{{ trans('text.author') }}</td>
							<td>{{ $resource->author }}</td>
						</tr>
						<!-- year -->
						<tr>
							<td class="text-lecture">{{ trans('text.year') }}</td>
							<td>{{ $resource->year }}</td>
						</tr>
						<!-- licence -->
						<tr>
							<td class="text-lecture">{{ trans('text.licence') }}</td>
							<td>
								@if ($resource->licences)
									@foreach($resource->licences as $licence)
										<img src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
									@endforeach
								@endif
							</td>
						</tr>
						<!-- length -->
						<tr>
							<td class="text-lecture">{{ trans('text.length') }}</td>
							<td>{{ $resource->length }}</td>
						</tr>
					</table>
				</div>
				<!-- file preview -->
				<div class="col-xs-6">
					@foreach ($resources_files as $resources_file)
						<div class="row">
							<div class="col-xs-12">
								
								<!-- VIDEO --> 
								@if ($resource->resource_type->name == "video")
									<video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
									    <source src="{{ $resources_file }}?{{ time() }}" type='video/mp4'>
									    <p class="vjs-no-js">
											To view this video please enable JavaScript, and consider upgrading to a web browser that
											<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
									    </p>
									</video>
								
								<!-- IMAGE -->
								@elseif($resource->resource_type->name == "image")
									<img src="{{ $resources_file }}?{{ time() }}" style="max-width: 100%;">
								
								<!-- NO PREVIEW -->
								@else
									<div class="no-preview">
										{{ trans('text.no_preview_available') }}
									</div>
								@endif 

							</div>
						</div>
					@endforeach
				</div>
				<!-- right -->
				<div class="col-xs-1">
					
				</div>
			</div>

			<!-- download footer -->
			<div class="row">
				<div class="col-xs-12 text-center">
					@foreach ($resources_files as $resources_file)
						<a href="{{ $resources_file }}" class="btn round_button bck-resource" download title="{{ $resources_file }}"><i class="fa fa-download" aria-hidden="true"></i></a>
					@endforeach
				</div>
			</div>

		</div>
	</div>
	

</div>