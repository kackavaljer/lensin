<?php 
	$languages = \App\Language::where('published', 1)->get();
	$switches = ['courses', 'lectures', 'tools', 'study_cases', 'projects'];
	$platforms_list = json_decode(central_server_data('/api/server_list'));
	$platforms = platforms_list();
	$this_server_id = server_property('server_id');
	$central_server = server_property('central_server');
	$active_switch = (isset($active_switch)) ? $active_switch : "";
	$resource_switch = (isset($_GET['res']) ? $_GET['res'] : []);
?>

<div class="left_filter_menu">
	<!-- TEXT FILTER -->
	<div class="dropdown text-center">
		<div class="dropdown-toggle dropdown_selector" id="dropdownMenuLanguageLeft" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			{!! cur_lan_name() !!}
			<span class="caret"></span>
		</div>
		<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenuLanguageLeft">
			@foreach ($languages as $language)
				<li><a href="/locale/{!! $language->locale !!}">{!! $language->name !!}</a></li>
			@endforeach
		</ul>
	</div>

	<!-- RESOURCE SWITCHES -->
	<h4 class="resources_title">{{ strtoupper(trans('text.resources')) }}</h4>

	@foreach ($switches as $switch)
		<?php 

			$checked = "";
			
			// if home page
			if (isset($home_page)) {

				$checked = "checked";

				// if any checked
				if ($resource_switch) {

					$checked = "";
					// if thso switch is checked
					if (in_array($switch, $resource_switch)) {
						$checked = "checked";
					}

				}
			
			} else {

				if ($active_switch == $switch) {
					$checked = "checked";
				}
			}

		?>
		

		<label class="switch">
		  <input type="checkbox" id="{!! $switch !!}_switch" class="resources" value="{!! substr($switch, 0, -1) !!}" {!! $checked !!}>
		  <div class="slider round {!! $switch !!}_slider"></div>
		</label>
		<div class="resource_name" id="{!! $switch !!}_title">{{ strtoupper(trans('text.' . $switch)) }}</div>
	@endforeach
	
	<h4 class="resources_title" style="margin-top: 30px;">{{ strtoupper(trans('text.platform')) }}</h4>

		<!-- PLATFORM SWITCHES FOR CENTRAL -->
		<?php $platform_list = json_decode(central_server_data('/api/server_list')); ?>

		@foreach ($platform_list as $platform)
			@if ($platform->id == $this_server_id)
				<label class="switch_platform" >
					<input type="checkbox" class="platforms" id="platform_{!! $platform->id !!}" value="{!! $platform->name !!}" data-server-id={!! $platform->id !!} checked>
				    <div class="slider_platform round platform_{!! $platform->id !!}_slider" title="{{ isset($platform->server_description) ? $platform->server_description : "" }}"></div>
				</label>
				<div class="platform_name" id="{!! $platform->name !!}_platform">{!! $platform->name !!}</div>
			@endif 
		@endforeach 

		@foreach ($platform_list as $platform)
			@if ($platform->id != $this_server_id)
				<?php 
					$checked = (server_property('central_server')) ? "checked" : "";
					if (isset($_GET['platforms']) && (server_property('central_server'))) {
						$checked = "checked";
						if (isset($_GET['platforms']) == $platform->id) {
							$checked = "";	
						}
						
					}
				?>
				<label class="switch_platform" >
					<input type="checkbox" class="platforms" id="platform_{!! $platform->id !!}" value="{!! $platform->name !!}" data-server-id={!! $platform->id !!} {!! $checked !!}>
                    <div class="slider_platform round platform_{!! $platform->id !!}_slider" title="{{ isset($platform->server_description) ? $platform->server_description : "" }}"></div>
				</label>
				<div class="platform_name" id="{!! $platform->name !!}_platform">{!! $platform->name !!}</div>
			@endif 
		@endforeach

</div>

<script type="text/javascript">
	
	// FILTER DATATABLE RESOURCES
	$('.resources').click(function() {

		// if the page is front page 
		@if (isset($home_page)) 
			var resources = "";

			$('.resources:checkbox:checked').each(function() {
				resources += $(this).val() + "|";
			});

			// remove the last "|"
			resources = resources.substring(0, resources.length - 1);

			// search the data and redraw table
			table.column( 6 ).search(resources, true).draw();
		
		// ELSE IF OTHER PAGES
		@else  
			window.location = "{!! url('/') !!}?res[]=" + $(this).val() + "s";
		@endif 
	});


	// FILTER DATATABLE PLATFORMS
	$('.platforms').click(function() {

		@if (server_property('central_server') != 1)
			
			var platforms = "";

			$('.platforms:checkbox:checked').each(function() {
				platforms += '&platforms[]=' + $(this).data('server-id');
			});

			location.href = "{!! server_property('central_server_address') !!}?" + platforms;

		@else 

			var platforms = "";

			$('.platforms:checkbox:checked').each(function() {
				platforms += $(this).val() + "|";
			});

			// remove the last "|"
			platforms = platforms.substring(0, platforms.length - 1);

			// search the data and redraw table
			table.column( 7 ).search(platforms, true).draw();
		@endif
	});	

  $( function() {
    $( "#slider" ).slider({
      value:0,
      min: 0,
      max: 1,
      step: 1,
      animate:true,
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.value );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider" ).slider( "value" ) );
  } );



function init_datatable_parameters(table) {

	var resources = "";

	$('.resources:checkbox:checked').each(function() {
		resources += $(this).val() + "|";
	});

	// remove the last "|"
	resources = resources.substring(0, resources.length - 1);

	// search the data and redraw table
	table.column( 6 ).search(resources, true).draw();

}  
  </script>
