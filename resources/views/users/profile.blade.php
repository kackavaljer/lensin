@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-9">
            <h2>{{ trans('text.user_profile') }}</h2>

            <!-- name --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.name') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="name" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->name }}" placeholder="{{ trans('text.insert_name') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- last_name --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.last_name') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="last_name" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->last_name }}" placeholder="{{ trans('text.insert_last_name') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- user_type --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.user_type') }}
                </div>
                <div class="col-xs-10">
                    {!! Form::select('user_type_id', $user_types, $user->user_type_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) !!}
                </div>
            </div>                
            <!-- school --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.school') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="school" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->school }}" placeholder="{{ trans('text.insert_school') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- departement --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.departement') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="departement" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->departement }}" placeholder="{{ trans('text.insert_departement') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- position --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.position') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="position" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->position }}" placeholder="{{ trans('text.insert_position') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- address --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.address') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="address" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->address }}" placeholder="{{ trans('text.insert_address') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- country --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.country') }}
                </div>
                <div class="col-xs-10">
                    {!! Form::select('country_id', $countries, $user->country_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) !!}
                </div>
            </div>
            <!-- interrest --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.interest') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="interest" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->interest }}" placeholder="{{ trans('text.insert_interest') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- phone --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.phone') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="phone" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->phone }}" placeholder="{{ trans('text.insert_phone') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>
            <!-- web --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.web') }}
                </div>
                <div class="col-xs-10">
                    <input  type="text" name="web" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->web }}" placeholder="{{ trans('text.insert_web') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>  
            <!-- email --> 
            <div class="row">
                <div class="col-xs-2">
                    {{ trans('text.email') }}
                </div>
                <div class="col-xs-10">
                    <input  type="email" name="email" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->email }}" placeholder="{{ trans('text.insert_email') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                </div>
            </div>

        </div>

        <!-- profile image --> 
        <div class="col-md-3">
            <div class="row">
                <div class="col-xs-12">
                    <h3>{{ trans('text.profile_image') }}
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12">

                    <div class="image_container" id='image_container'>
                        @if ($user->image)
                            <div class="image_container_inner">
                                <img src="{{ $user->image }}" style="width:100%;">
                                <!-- bottom popup -->
                                <div class="image_change_bottom_cover bck-course">
                                    <!-- remove image button -->
                                    <button class="btn btn-danger remove_user_image_button" data-id="1" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                                </div>          
                            </div>
                        @else 
                            <div id="upload_button"><br><label for="course_input">{{ trans('text.drop_or_click_to_select_image') }}</label></div>
                        @endif
                        
                    </div>

                </div>
            </div>

        </div>


    </div>
</div>

<script type="text/javascript">
    // UPLOAD COURSE COVER IMAGE
    var uploader = new ss.SimpleUpload({
        dropzone:'image_container',
        //button:'image_container',
        url: '/profile/upload_image/{{ $user->id }}', // URL of server-side upload handler
        name: 'image', // Parameter name of the uploaded file
        data: {
            "_token" : $("meta[name='csrf-token']").attr("content")
        },
        onComplete: function(filename, response) {
            if (!response) {
                alert(filename + 'upload failed');
                return false;            
            }
            $("#image_container").html(response);
        },
        onSubmit: function() {
            $("#course_image_div").html("{{ trans('text.uploading')}}"); 
        },

    });


    // REMOVE user image
    $('body').on('click', '.remove_user_image_button', function(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        var button = $(this);

        $.ajax({
            url: '/profile/remove_image/{!! Auth::user()->id !!}',
            type: 'POST',
            data: {
                '_token': token,
            },
            success: function(data, status) {
                $("#image_container").html("<br>" + "{{ trans('text.drop_or_click_to_select_image') }}");
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
    })


</script>


@endsection
