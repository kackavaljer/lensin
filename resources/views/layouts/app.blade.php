<?php
App::setLocale(Session::get('locale'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! server_property('platform_title') !!}</title>
    @include('default.head')

</head>

<body>
    
    @include('flash::message')

    <div id="wrapper" style="padding-top: 20px;">

        <!-- HEADER --> 
        <header>
            
            <!-- SMALL SCREENS -->
            <div class="container visible-xs-block">
                <div class="navbar-header">
                    <!-- NAV BAR -->
                    <div class="col-xs-3">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                        
                        </button>
                    </div>
                    <!-- LOGO -->
                    <div class="col-xs-6">
                        <a href='/'><img src="/images/logo.png?{!! time() !!}" class="header-logo"></a><br>
                    </div>
                    <!-- LANGUAGE DROPDOWN -->
                    <?php $languages = \App\Language::where('published', 1)->get(); ?>
                    <div class="col-xs-3">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                {{ strtoupper(app()->getLocale()) }}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenuLanguage">
                                @foreach ($languages as $language)
                                    <li><a href="/locale/{!! $language->locale !!}">{!! $language->name !!}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- LARGE SCREEN -->
            <div class="container hidden-xs">
                <div class="row">
                    <!-- LOGO -->
                    <div class="col-xs-2 text-center">
                        <a href="/"><img src="/images/logo.png" class="header-logo"></a>
                        <div class="header-logo-title">{{ server_property('slogan') }}</div>
                    </div>
                    <!-- SEARCH -->
                    <div class="col-xs-4">
                        <div>
                            <input type="text" class="form-control header-search"  placeholder="&#xF002; {{ trans('text.learn_about') }}" />
                        </div>
                        <div class="dropdown">
                            <span class="header-advanced-search">
                                {{ trans('text.advanced_research') }}
                                <span class="caret"></span>
                            </span>

                            <div class="advanced_search_div">
                                @include('home_advanced_menu')
                            </div>
                        </div>                        
                    </div>
                    <!-- MENU -->
                    <div class="col-xs-6" style="text-align: right;">
                        <!-- menu if user is logged in -->
                        @if (Auth::check())
                            <div class="dropdown">
                                <!-- user image -->
                                <div class="header-menu-profile dropdown-toggle" id="dropdownUser"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background-image: url('{{ user_image(Auth::user()->id) }}');"></div>
                                <!-- user menu -->
                                <ul class="dropdown-menu pull-right profile_links" aria-labelledby="dropdownUser">
                                    <!-- profile -->
                                    <li><a href="/profile">{{ trans('text.profile') }}</a></li>
                                    <!-- logout -->
                                    <li><a href="/logout">{{ trans('text.logout') }}</a></li>
                                    <!-- backend if admin -->
                                    @if (Auth::user()->hasGroup('admin'))
                                        <li><a href="/backend">{{ trans('text.backend') }}</a></li>
                                    @endif
                                </ul>
                            </div>

                        @else
                            <!-- menu if user is NOT logged in -->
                            <a href="/login">
                                <div class="header-menu-profile" style="background-image: url('{{ user_image() }}');"></div>
                            </a>
                        @endif
                        <!-- menu icons -->
                        <div class="header-menu-icons">
                            <a href="/contact">
                                <img src="/images/header-menu/contact.png">
                                <br>{{ strtoupper(trans('text.contact')) }}
                            </a>
                        </div>
                        <div class="header-menu-icons">
                            <a href="/tutorial">
                                <img src="/images/header-menu/tutorials.png">
                                <br>{{ strtoupper(trans('text.tutorial')) }}
                            </a>
                        </div>
                        <div class="header-menu-icons">
                            <a href="/network">
                                <img src="/images/header-menu/network.png">
                                <br>{{ strtoupper(trans('text.network')) }}
                            </a>
                        </div>
                        <div class="header-menu-icons">
                            <a href="/labs">
                                <img src="/images/header-menu/labs.png">
                                <br>{{ strtoupper(trans('text.labs')) }}
                            </a>
                        </div>
                        <div class="header-menu-icons">
                            <a href="/about">
                                <img src="/images/header-menu/about.png">
                                <br>{{ strtoupper(trans('text.about')) }}
                            </a>
                        </div>                        
                    </div>
                </div>
            </div>

        </header>

        <!-- CONTENT -->
        <article>
            @yield('content')
        </article>
        
        <!-- FOOTER --> 
        @include('default.footer')


    </div><!-- #wrapper -->

    <!-- MODAL -->
    <div id="main_modal" class="modal fade" role="dialog">
    </div>

</body>
</html>

   @if (session()->has('message'))
        <script>
            alert('{{ session('message') }}');
        </script>
    @endif  

    @if (isset($errors))
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <script>
                        alert('{{ $error }}');
                    </script>
                @endforeach
            </div>
        @endif 
    @endif

<script type="text/javascript">
    $( document ).ready(function() {


    });
</script>