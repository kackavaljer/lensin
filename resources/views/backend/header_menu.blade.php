@if (Auth::user()->hasRole('manageservers') && server_property('central_server') == 1)
	<a href="/backend/servers"><div class="btn bck-darkgray">{{ trans('text.servers') }}</div></a>
@endif
@if (Auth::user()->hasRole('managesite'))
	<a href="/backend/site"><div class="btn bck-darkgray">{{ trans('text.site') }}</div></a>
@endif
@if (Auth::user()->hasRole('manageusers'))
	<a href="/backend/users"><div class="btn bck-darkgray">{{ trans('text.users') }}</div></a>
@endif
@if (Auth::user()->hasRole('managelanguages') && server_property('central_server') == 1)
	<a href="/backend/languages"><div class="btn bck-darkgray">{{ trans('text.languages') }}</div></a>
@endif
