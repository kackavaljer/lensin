@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- CONTENT -->
			<div class="col-xs-12">

                <!-- content header -->
                <div class="row content-header">
                    <!-- middle -->
                    <div class="col-xs-12 text-center">
                    	@include('backend.header_menu')
                    </div>
        
                </div>

                <!-- DATATABLE USERS -->
			    <table class="display row-links" id="datatable">
			        <thead>
			            <tr>
			                <th>{{ trans('text.name') }}</th>
			                <th>{{ trans('text.last_name') }}</th>
			                <th>{{ trans('text.email') }}</th>
			            </tr>
			        </thead>
			    </table>

			</div>
		</div>
	</div>

<script type="text/javascript">

	var values = [];
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            "cache":true,
            "url" : '/users/list',
            "rowId": 'id',
        },
        "columns": [
            { "data": "name" },
            { "data": "last_name" },
            { "data": "email" },
        ]
    });

    // Open course on click     
    $('#datatable tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        location.href = '/backend/users/edit/' + data['id'];
    });

</script>    
@endsection
