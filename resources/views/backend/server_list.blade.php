@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- CONTENT -->
			<div class="col-xs-12">

                <!-- content header -->
                <div class="row content-header">
                    <!-- middle -->
                    <div class="col-xs-12 text-center" style="padding-bottom: 5px;">
                    	@include('backend.header_menu')
                    </div>
                </div>
			</div>
		</div>

        <br>
      
        <!-- ADD A NEW SERVER -->
        <div class="title">{{ strtoupper(trans('text.add_new_server')) }}</div>
        <form action="/backend/servers/create" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-xs-1">
                    <small>{{ trans('text.enabled') }}</small><br>
                    <input type="checkbox" name="enabled" value="1" >
                </div>
                <div class="col-xs-2">
                    <small>{{ trans('text.name') }}</small><br>
                    <input type="text" name="name" class="form-control" required>
                </div>
                <div class="col-xs-2">
                    <small>{{ trans('text.address') }}</small><br>
                    <input type="text" name="address"  class="form-control" required>
                </div>
                <div class="col-xs-2">
                    <small>{{ trans('text.administrator_contact') }}</small><br>
                    <input type="text" name="administrator_contact" class="form-control" required>
                </div>
                <div class="col-xs-2">
                    <small>{{ trans('backend.platform_description') }}</small><br>
                    <input type="text" name="server_description" class="form-control" required>
                </div>                
                <div class="col-xs-2">
                    <small>{{ trans('text.notes') }}</small><br>
                    <input type="text" name="notes"  class="form-control">
                </div>   
                <div class="col-xs-1">
                    <br>
                    <button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
                </div>
            </div>
        </form>

        <hr>        
        <br>
        <div class="title">{{ strtoupper(trans('text.server_list')) }}</div>
        

        @foreach($server_list as $server) 
            <form action="/backend/servers/update/{!! $server->id !!}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-1 text-center">
                        <small>{{ trans('text.enabled') }} / ID</small><br>
                        <input type="checkbox" name="enabled" {{ $server->enabled ? "checked" : ""}}>
                        {!! $server->id !!} 
                    </div>                
                    <div class="col-xs-2">
                        <small>{{ trans('text.name') }}</small><br>
                        <input type="text" name="name" value="{{ $server->name }}" class="form-control">
                    </div>
                    <div class="col-xs-1">
                        <small>{{ trans('text.address') }}</small><br>
                        <input type="text" name="address" value="{{ $server->address }}" class="form-control">
                    </div>

                    <div class="col-xs-2">
                        <small>{{ trans('text.administrator_contact') }}</small><br>
                        <input type="text" name="administrator_contact" value="{{ $server->administrator_contact }}" class="form-control">
                    </div>    
                    
                    <div class="col-xs-2">
                        <small>{{ trans('backend.platform_description') }}</small><br>
                        <input type="text" name="server_description" value="{{ $server->server_description }}" class="form-control">
                    </div>    
                    
                    <div class="col-xs-2">
                        <small>{{ trans('text.notes') }}</small><br>
                        <input type="text" name="notes" value="{{ $server->notes }}" class="form-control">
                    </div>   
                    <div class="col-xs-1">
                        <br>
                        <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                    </div>
                    <div class="col-xs-1">
                        <br>
                        <a href="/backend/servers/delete/{!! $server->id !!}"><button type="button" class="btn btn-danger">{{ trans('text.delete') }}</button></a>
                    </div>
                </div>
            </form>
        @endforeach
        

	</div>

@endsection
