@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- CONTENT -->
			<div class="col-xs-12">

                <!-- content header -->
                <div class="row content-header">
                    <!-- middle -->
                    <div class="col-xs-12 text-center" style="padding-bottom: 5px;">
                    	@include('backend.header_menu')
                    </div>
                </div>
			</div>
		</div>

        <br>
      
        <!-- ADD A NEW LANGUAGE -->
        <div class="title">{{ strtoupper(trans('languages.add_new_language')) }}</div>
        <form action="/backend/languages/create" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <!-- locale -->
                <div class="col-xs-2">
                    <small>{{ trans('languages.locale') }}</small><br>
                    <input type="text" name="locale"  class="form-control" required maxlength="2">
                </div>
                <!-- name -->
                <div class="col-xs-9">
                    <small>{{ trans('languages.name') }}</small><br>
                    <input type="text" name="name" class="form-control" required>
                </div>
                <!-- create -->
                <div class="col-xs-1">
                    <br>
                    <button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
                </div>
            </div>
        </form>

        <hr>        
        <br>
        <div class="title">{{ strtoupper(trans('languages.language_list')) }}</div>
        

        @foreach($languages as $language) 
            <form action="/backend/languages/update/{!! $language->id !!}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <!-- published -->
                    <div class="col-xs-1 text-center">
                        <small>{{ trans('text.published') }}</small><br>
                        <input type="checkbox" name="published" {{ $language->published ? "checked" : ""}} value="1">
                    </div>       
                    <!-- locale -->
                    <div class="col-xs-2">
                        <small>{{ trans('languages.locale') }}</small><br>
                        <input type="text" name="locale" value="{{ $language->locale }}" class="form-control">
                    </div>
                    <!-- name -->         
                    <div class="col-xs-6">
                        <small>{{ trans('languages.name') }}</small><br>
                        <input type="text" name="name" value="{{ $language->name }}" class="form-control">
                    </div>
                    <!-- save -->
                    <div class="col-xs-1">
                        <br>
                        <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                    </div>
                    <!-- delete -->
                    <div class="col-xs-1">
                        <br>
                        <a href="/backend/languages/delete/{!! $language->id !!}"><button type="button" class="btn btn-danger">{{ trans('text.delete') }}</button></a>
                    </div>
                    <!-- manage -->
                    <div class="col-xs-1">
                        <br>
                        <a href="/backend/translations/{!! $language->id !!}"><button type="button" class="btn btn-info">{{ trans('text.manage') }}</button></a>
                    </div>
                </div>
            </form>
        @endforeach

        <br>

        <!-- DISTRIBUTE TRANSLATIONS TO LOCAL PLATFORMS -->
        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-success" id="distribute">{{ trans('backend.distribute_translations_to_local_platforms') }}</button>
            </div>
        </div>
        

	</div>

<script type="text/javascript">

    // distribute languages across platforms
    $('#distribute').click(function (){
        
        // prepare the results array
        var results = [];
        var button = $(this);
        //disable the button
        button.attr("disabled", true);
        button.html("<i class='fa fa-spinner fa-pulse fa-fw'></i> {{ trans('text.please_wait') }} ");

            // trigger pull on servers
            $.when(

                @foreach( $platforms as $platform)

                    $.get("{{ $platform->address }}/backend/synchronize?central_server_address={!! url("/") !!}", function(html) {
                        results.push(html);
                    })
                    // if the loop is last - no comma 
                    @if (!$loop->last)
                        ,
                    @endif

                @endforeach

            ).then(function() {
                //display the results
                button.html("{{ trans('backend.distribute_translations_to_local_platforms') }}");
                button.removeAttr("disabled");
                bootbox.alert(results.toString());

            });

    });
    
        
    
</script>

@endsection
