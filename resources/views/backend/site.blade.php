@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- CONTENT -->
			<div class="col-xs-12">

                <!-- content header -->
                <div class="row content-header">
                    <!-- middle -->
                    <div class="col-xs-12 text-center">
                    	@include('backend.header_menu')
                    </div>
                </div>
			</div>
		</div>

        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>{{ trans('backend.platform_properties') }}</h2>
            </div>
        </div>
        <!-- SERVER LOGO --> 
        <form action="/backend/upload/logo" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-3">
                    {{ trans('text.server_logo') }} {!! tooltip(trans('text.tooltip_server_logo')) !!}
                </div>
                <div class="col-xs-3">
                    <img src="/images/logo.png?{!! time() !!}" style="max-height: 40px; max-width: 200px;">
                </div>
                <div class="col-xs-3">
                    <input type="file" name="image">
                </div>
                <div class="col-xs-3 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        


        <!-- SERVER ROUND LOGO --> 
        <form action="/backend/upload/round_logo" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-3">
                    {{ trans('text.server_round_logo') }} {!! tooltip(trans('text.tooltip_server_round_logo')) !!}
                </div>
                <div class="col-xs-3">
                    <img src="/images/server/logo.png?{!! time() !!}" style="max-height: 40px; max-width: 200px;">
                </div>
                <div class="col-xs-3">
                    <input type="file" name="image">
                </div>
                <div class="col-xs-3 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        

        <!-- SERVER ROUND LOGO TRANSPARENT --> 
        <form action="/backend/upload/round_logo_transparent" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-3">
                    {{ trans('text.server_round_transparent_logo') }} {!! tooltip(trans('text.tooltip_server_round_logo_transparent')) !!}
                </div>
                <div class="col-xs-3">
                    <img  style="background: #CCC;" src="/images/server/logo_transparent.png?{!! time() !!}" style="max-height: 40px; max-width: 200px;">
                </div>
                <div class="col-xs-3">
                    <input type="file" name="image">
                </div>
                <div class="col-xs-3 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        


        <!-- TITLE --> 
        <?php $platform_title = server_property('platform_title'); ?>
        <form action="/backend/site" method="POST">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-3">
                    {{ trans('text.platform_title') }}
                </div>
                <div class="col-xs-7">
                    <input type="text" name="data" class="form-control" value="{{ $platform_title }}">
                    <input type="hidden" name="name" value="platform_title">
                </div>
                <div class="col-xs-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>     

 
        <!-- TEXT UNDER THE LOGO --> 
        <?php $slogan = server_property('slogan'); ?>
        <form action="/backend/site" method="POST">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-3">
                    {{ trans('text.slogan') }}
                </div>
                <div class="col-xs-7">
                    <input type="text" name="data" class="form-control" value="{{ $slogan }}">
                    <input type="hidden" name="name" value="slogan">
                </div>
                <div class="col-xs-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>      


        <!-- CENTRAL SERVER PROPERTIES --> 
        <div class="row padding-small hover">
            <div class="col-xs-12 text-center">
                <h3>{{ trans('backend.central_server_properties') }}</h3>
            </div>
        </div>

        <!-- CENTRAL SERVER ADDRESS --> 
        <form action="/backend/site" method="POST">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-3">
                    {{ trans('text.central_server_address') }}
                </div>
                <div class="col-xs-7">
                    <input type="text" name="data" class="form-control" value="{{ server_property('central_server_address') }}">
                    <input type="hidden" name="name" value="central_server_address">
                </div>
                <div class="col-xs-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        

        <!-- REGISTRER ON CENTRAL SERVER -->
        @if (!server_property('central_server'))
            <div class="row padding-small hover">
                <div class="col-xs-6">
                    @if (server_property('server_id'))
                        {{ trans('text.server_id') }}: {!! server_property('server_id') !!}
                    @else 
                        {{ trans('backend.you_need_to_register_your_platform_on_central_server') }}
                    @endif
                </div>
                <div class="col-xs-6 text-right">
                    <a href="/backend/register_platform_request"><button type="button" class="btn btn-success">{{ trans('backend.register_platform_on_central_server') }}</button></a>
                </div>
            </div>
        @endif
	</div>

@endsection
