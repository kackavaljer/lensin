@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- CONTENT -->
			<div class="col-xs-12 text-center" style="min-height: 300px;">

                <!-- content header -->
                <div class="row content-header">
                    <!-- middle -->
                    <div class="col-xs-12 text-center">
                        @include('backend.header_menu')
                    </div>
        
                </div>


                <h2>{{ trans('text.welcome_to_backend') }}

			</div>
		</div>
	</div>

 
@endsection
