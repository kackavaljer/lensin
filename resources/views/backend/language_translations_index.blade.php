@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- CONTENT -->
			<div class="col-xs-12">

                <!-- content header -->
                <div class="row content-header">
                    <!-- middle -->
                    <div class="col-xs-12 text-center" style="padding-bottom: 5px;">
                    	@include('backend.header_menu')
                    </div>
                </div>
			</div>
		</div>
      
        <div class="title text-center"><h2>{{ $language->name }}</h2></div>
        
        <div class="row">
            <div class="col-xs-12">

                <table class="table">
                    <thead>
                        <tr>
                            <th>{{ trans('languages.group') }}</th>
                            <th>{{ trans('languages.name') }}</th>
                            <th>{{ trans('languages.text') }}</th>
                            <th></th>
                        </tr>
                    </thead>
                    @foreach($translations as $translation)

                            <tr>
                                <!-- group -->
                                <td>
                                    <form action="/backend/translations/update/{!! $translation->id !!}" method="POST">
                                        {{ csrf_field() }}
                                        {{ $translation->group }}    
                                </td>
                                <!-- name -->
                                <td>
                                    {{ $translation->item }}
                                </td>
                                <!-- text -->
                                <td>
                                    <input type="text" name="text" value="{{ $translation->text }}" class="form-control">
                                </td>        
                                <!-- save -->
                                <td>
                                        <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                                    </form>
                                </td>                    
                            </tr>
                        
                    @endforeach
                   
                </table>
            </div>
        </div>
        

	</div>

<script type="text/javascript">

$(document).ready(function() {
    $('.table').DataTable({
        stateSave: true
    });
} );

</script>

@endsection
