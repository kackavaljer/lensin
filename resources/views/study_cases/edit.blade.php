@extends('layouts.resource')

@section('content')

<div class="resource_header bck-study-case">
	<table style="margin-left: auto; margin-right: auto;">
		<tr>
			<td style="padding: 5px;">
				<a href="/">
					<button class="btn btn-danger back-button" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
				</a>
				<br>
				{{ trans('text.back') }}
			</td>
		</tr>
	</table>			
</div>

<div class="container">
	<!-- HEADER BUTTONS -->
	<div class="row">
		<div class="col-xs-12 text-center">
			<ul id="content_tabs">
				<li class="btn nav-course active"><a data-toggle="tab" href="#study_case_studies_tab">{{ strtoupper(trans('text.studies')) }}</a></li>
			    <li class="btn nav-course"><a data-toggle="tab" href="#study_case_criteria_tab">{{ strtoupper(trans('text.criteria')) }}</a></li>
			</ul>

		</div>
	</div>

	<!-- STUDY CASE TITLE  -->
	<div class="row" style="margin-top: 20px">
		<div class="col-xs-3">
			<img src="/images/resource/case.png" class="table_view_image"> {{ strtoupper(trans('text.study_case_title')) }}
		</div>
		<div class="col-xs-9">
			<input type="text" name="name" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
		</div>
	</div>


	<div class="row tab-content">
	
		<!-- STUDY CASE INFO -->
		<div role="tabpanel" class="tab-pane active" id="study_case_studies_tab">

			<div class="row" style="margin-top: 20px;">
				
				<!-- LEFT -->
				<div class="col-xs-4 text-center">

					<!-- IMAGE -->
					{{ strtoupper(trans('text.cover_image')) }}
					<div class="image_container" id='image_container'>
						<!-- show image if exists -->
						@if ($study_case->image)
							@include('study_cases.cover_image', ['study_case' => $study_case])
						@else 
							<!-- show upload button -->
							<div id="upload_button"><br><label for="study_case_input">{{ trans('text.drop_or_click_to_select_image') }}</label></div>
						@endif
					</div>

					<!-- CATEGORY -->
					{{ trans('text.category') }}
					<input list="categories" type="text" id="category" name="category" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->category }}" class="form-control update_input" required>
					<datalist id="categories">
						@foreach (list_study_case('category') as $category) 
							<option value="{{ $category->category }}">
						@endforeach
					</datalist>

					<!-- PRODUCER -->
					{{ trans('text.producer') }}
					<input list="producers" type="text" id="producer" name="producer" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->producer }}" class="form-control update_input" required>
					<datalist id="categories">
						@foreach (list_study_case('producer') as $list_data) 
							<option value="{{ $list_data->producer }}">
						@endforeach
					</datalist>

					<!-- DESIGNER -->
					{{ trans('text.designer') }}
					<input list="designers" type="text" id="designer" name="designer" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->designer }}" class="form-control update_input" required>
					<datalist id="designers">
						@foreach (list_study_case('designer') as $list_data) 
							<option value="{{ $list_data->designer }}">
						@endforeach
					</datalist>

					<!-- COUNTRY -->
					{{ trans('text.country') }}
					{!! Form::select('country_id', $countries, $study_case->country_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'study_cases', 'data-id'=> "$study_case->id" ]) !!}
					
					<!-- LOCATION -->
					{{ trans('text.location') }}
					<input list="locations" type="text" id="location" name="location" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->location }}" class="form-control update_input" required>
					<datalist id="locations">
						@foreach (list_study_case('location') as $list_data) 
							<option value="{{ $list_data->location }}">
						@endforeach
					</datalist>

					<!-- YEAR -->
					{{ trans('text.year') }}
					<input type="text" id="year" name="year" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->year }}" class="form-control update_input" required maxlength="4">
					
					<!-- EMAIL -->
					{{ trans('text.email') }}
					<input type="email" id="location" name="email" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->email }}" class="form-control update_input" required>
					
					<!-- WEBSITE -->
					{{ trans('text.website') }}
					<input list="website" type="text" id="website" name="website" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->website }}" class="form-control update_input" required>

						
				</div>

				<!-- RIGHT -->
				<div class="col-xs-8">

					<!-- DESCRIPTION -->
					{{ strtoupper(trans('text.description')) }}<br>
					<textarea name="description" class="form-control update_input"  data-model='study_cases' data-id={{ $study_case->id }} placeholder="{{ trans('text.insert_description') }}" style="height: 300px;">{{ $study_case->description }}</textarea>
					<br>

					<!-- WEBSITE -->
					{{ strtoupper(trans('text.sustainable_benefits')) }}<br>
					<input type="text" id="benefits" name="benefits" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->benefits }}" class="form-control update_input" required>
					<br>

					<!-- IMAGES --> 
					

					<!-- UPLOAD REPORT --> 
					{{ strtoupper(trans('text.report')) }}
					<br>
					<span id="report_container">
						@if ($study_case->report)
							<a href="{!! $study_case->report !!}" download target="_blank">{!! $study_case->report !!}</a>
						@endif 
					</span>
					<br>
					<button class="btn bck-study-case" type="button" id="report_upload_button">{{ trans('text.click_to_select_file') }}</button>
					<button class="btn bck-study-case ajax_btn" type="button" data-url="/study_cases/report_delete" data-id="{!! $study_case->id !!}" data-return_fn="report_delete" data-confirm_text="{{ trans('text.delete_report_confirmation') }}">{{ trans('text.delete_report') }}</button>
				</div>
			</div>
		</div>

		<!-- CRITERIA TAB -->
		<div role="tabpanel" class="tab-pane" id="study_case_criteria_tab">
			@include('study_cases.edit_criteria_tab', ['guidelines' => $guidelines, 'study_case' => $study_case])
		</div>				

	</div>
</div>


<!-- Authors Modal -->
<div id="authorsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ trans('text.select_author') }}</h4>
			</div>
			<div class="modal-body">
				<table class="display" id="datatable" style="width: 100%">
					<thead>
						<th>{{ trans('text.name') }}</th>
						<th>{{ trans('text.last_name') }}</th>
						<th>{{ trans('text.school') }}</th>
						<th></th>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>


<!-- Resource Modal -->
<div id="resourceModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ strtoupper(trans('text.add_resource_information')) }}</h4>
			</div>
			<div class="modal-body" id="resource_modal_div">
				
			</div>
		</div>

	</div>
</div>


<script>

// DELETE THE REPORT LINK
function report_delete(data) {
	$('#report_container').html("");
}

$( document ).ready(function() {


	// REMOVE COURSE IMAGE
	$('body').on('click', '.remove_study_case_cover_image_button', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var button = $(this);

		$.ajax({
			url: '/study_cases/remove_image/' + {{ $study_case->id }},
			type: 'POST',
			data: {
				'_token': token,
		      },
			success: function(data, status) {
				$("#image_container").html("<br>" + "{{ trans('text.drop_or_click_to_select_image') }}");
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});

	// UPLOAD STUDY CASE IMAGE
	
	var uploader = new ss.SimpleUpload({
		dropzone:'image_container',
		url: '/study_cases/cover_image_upload/{{ $study_case->id }}', // URL of server-side upload handler
		name: 'image', // Parameter name of the uploaded file
		data: {
			"_token" : $("meta[name='csrf-token']").attr("content")
		},
		onComplete: function(filename, response) {
			if (!response) {
            	alert(filename + 'upload failed');
            	return false;            
        	}
        	$("#image_container").html(response);
        },
        onSubmit: function() {
            $("#image_container").html("{{ trans('text.uploading')}}"); 
        }
	});



	// UPLOAD REPORT
	var upload_report = new ss.SimpleUpload({
		button:'report_upload_button',
		url: '/study_cases/report_upload/{{ $study_case->id }}', // URL of server-side upload handler
		name: 'report_file', // Parameter name of the uploaded file
		data: {
			"_token" : $("meta[name='csrf-token']").attr("content")
		},
		onComplete: function(filename, response) {
			if (!response) {
            	alert(filename + 'upload failed');
            	return false;            
        	}
        	$("#report_container").html(response);
        },
        onSubmit: function() {
            $("#report_container").html("{{ trans('text.uploading')}}"); 
        }
	});

});

</script>

@endsection