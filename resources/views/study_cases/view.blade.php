@extends('layouts.app')

@section('content')

	<div class="container" style="margin-top: 20px;">

		<!-- MENU LEFT -->
		<div class="col-xs-2">
			@include('left_menu_filter', ['active_switch' => "study_cases"])
		</div>
		<div class="col-xs-10">

			<!-- content header -->
	        <div class="row content-header">
	            
	            <!-- left button -->
	            <div class="col-xs-2">
	            </div>
	            
	            <!-- middle -->
	            <div class="col-xs-8 text-center">
					<ul id="content_tabs">
					  <li class="btn nav-study-case active"><a data-toggle="tab" href="#study_case_tab">{{ strtoupper(trans('text.study_case')) }}</a></li>
					  <li class="btn nav-study-case"><a data-toggle="tab" href="#criteria_tab">{{ strtoupper(trans('text.criteria')) }}</a></li>
					</ul>
					<script>
						$('#content_tabs a').click(function (e) {
						  	e.preventDefault()
						  	$(this).tab('show')
						})
					</script>
				</div>
	            
	            <!-- right -->
	            <div class="col-xs-2">
	                <!-- manage button -->
	                @include('home_manage_button')
	            </div>

	        </div>

			<!-- title -->
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-xs-12">
					<img src="/images/resource/study_case.png" class="table_view_image"> <span class="pill bck-study-case">{{ $study_case->name }}</span>
				</div>
			</div>

			<!-- CONTENT --> 
			<div class="row">
				
				<!-- LEFT -->
				<div class="col-xs-4">

					<!-- COVER IMAGE --> 
					@if (isset($study_case->image))
						{{ strtoupper(trans('text.cover_image')) }}
						<img src="{{ $study_case->image }}" class="image" style="margin-top: 10px; margin-bottom: 10px;">
					@endif

					<!-- IMAGES -->

					<!-- DATA --> 
					<table style="width: 100%;">
						{!! (isset($study_case->category)) ? view_td('category', $study_case->category) : "" !!}
						{!! (isset($study_case->producer)) ? view_td('producer', $study_case->producer) : "" !!}
						{!! (isset($study_case->designer)) ? view_td('designer', $study_case->designer) : "" !!}
						{!! (isset($study_case->location)) ? view_td('email', $study_case->location) : "" !!}
						{!! (isset($study_case->year)) ? view_td('email', $study_case->year) : "" !!}
						{!! (isset($study_case->web)) ? view_td('web', $study_case->web) : "" !!}
					</table>

				</div>

				<!-- MIDDLE CONTENT -->
				<div class="col-xs-8">
					<!-- top row -->
					<div class="row">
						<div class="col-xs-6">
							<div class="title">{{ strtoupper(trans('text.description')) }}</div>
						</div>
						<div class="col-xs-6 text-right text-study-case title">
							@if ($study_case->report)
								<a href="{!! $study_case->report !!}" class="text-study-case" target="_blank" download>
									{{ strtoupper(trans('text.report')) }} <span><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>
								</a>
							@endif
						</div>
					</div>

					<!-- Description -->
					{{ ($study_case->description) ? $study_case->description : "" }}
					
					<br>
					<br>

					<!-- Sustainable benefits -->
					<div class="title">{{ strtoupper(trans('text.sustainable_benefits')) }}</div>
					{{ ($study_case->benefits) ? $study_case->description : "" }}

					<br>
					<br>

					<!-- guidelines -->
					<div class="row text-study-case">
						<div class="col-xs-2">
							{{ strtoupper(trans('text.dimension')) }}
						</div>
						<div class="col-xs-2">
							{{ strtoupper(trans('text.criteria')) }}
						</div>
						<div class="col-xs-2">
							{{ strtoupper(trans('text.guideline')) }}
						</div>
						<div class="col-xs-2">
							{{ strtoupper(trans('text.subguideline')) }}
						</div>
					</div>
					@if (isset($study_case->guidelines_level_0))
						@foreach($study_case->guidelines_level_0 as $dimension)
							<div class="row">
								<div class="col-xs-6">
									{{ $dimension->name }}
								</div>
							</div>

							<!-- criteria -->
							@foreach ($dimension->children as $criteria)
								<div class="row">
									<div class="col-xs-2 text-right">
										<i class="fa fa-long-arrow-right text-study-case " aria-hidden="true"></i>
									</div>
									<div class="col-xs-6">
										{{ $criteria->name }}
									</div>
								</div>

								<!-- guideline -->
								@foreach ($criteria->children as $guideline)
									<div class="row">
										<div class="col-xs-4 text-right">
											<i class="fa fa-long-arrow-right text-study-case " aria-hidden="true"></i>
										</div>
										<div class="col-xs-6">
											{{ $guideline->name }}
										</div>
									</div>
									
									<!-- subguideline -->
									@foreach ($guideline->children as $subguideline)
										<div class="row">
											<div class="col-xs-6 text-right">
												<i class="fa fa-long-arrow-right text-study-case " aria-hidden="true"></i>
											</div>
											<div class="col-xs-6">
												{{ $subguideline->name }}
											</div>
										</div>
									@endforeach 

								@endforeach 

							@endforeach 

						@endforeach
					@endif
				</div>
			</div>

		</div>
	</div>


	

@endsection
