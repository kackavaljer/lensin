<div class="image_container_inner">
	<img src="{{ $study_case->image }}" style="width:100%;">
	<!-- bottom popup -->
	<div class="image_change_bottom_cover bck-course">
		<!-- remove image button -->
		<button class="btn btn-danger remove_study_case_cover_image_button" data-id="{{ $study_case->id }}" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
	</div>			
</div>