<li id="guideline_li_{!! $guideline->id !!}" class="criteria_li" style="padding-top: 10px; padding-bottom: 10px;">
	<div class="row">
		<!-- OFFSET -->
		@if($level > 0)
			<div class="col-xs-{!! $level + 1 !!}">
			</div>
		
			<div class="col-xs-{!! 12 - $level - 1 !!} sortable_div_{!! $level !!}" >
		@else 
			<div class="col-xs-12 sortable_div_{!! $level !!}" >
		@endif		

		<!-- TEXT -->
			<input type="text" name="name" data-model='study_case_guideline' data-id={{ $guideline->id }} value="{{ $guideline->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
			<ul data-level="{!! $level !!}" data-guideline-id="{!! $guideline->id !!}" id="guideline_ul_{!! $guideline->id !!}" class="sortable_ul">

				<!-- load children -->
				@if ($guideline->children)
					@foreach($guideline->children as $child)
						@include('study_cases_guidelines.edit', ['guideline' => $child, 'level' => $level + 1])
					@endforeach
				@endif

			</ul>

			<!-- BUTTON + -->
			<div class="row">
				<div class="col-xs-12">
					@if ($level < 3)
						<div class="btn round_button btn-success create_guideline" data-guideline-id = '{!! $guideline->id !!}' data-level="{!! $level !!}"><i class="fa fa-plus" aria-hidden="true"></i></div>
					@endif
						<div class="btn round_button btn-danger delete_guideline" data-guideline-id = '{!! $guideline->id !!}' data-level="{!! $level !!}"><i class="fa fa-times" aria-hidden="true"></i></div>
				</div>
			</div>	
	

		</div>

	</div>

</li>
