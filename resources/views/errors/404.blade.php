@extends('layouts.app')

@section('content')

	<div class="container text-center" style="padding-top: 100px; padding-bottom: 200px;">
		<h1>This page does not exist.</h1>
		You can return to the home page <a href="/">here.</a> 
	</div>

@endsection
