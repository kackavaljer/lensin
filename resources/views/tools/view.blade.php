@extends('layouts.app')

@section('content')

	<div class="container" style="margin-top: 20px;">

		<!-- MENU LEFT -->
		<div class="col-xs-2">
			@include('left_menu_filter', ['active_switch' => "tools"])
		</div>

		<!-- CONTENT -->
		<div class="col-xs-10">

			<div class="row">
	
				<!-- content header -->
		        <div class="row content-header">
		            
		            <!-- left button -->
		            <div class="col-xs-2">
		            </div>
		            
		            <!-- middle -->
		            <div class="col-xs-8 text-center">
					</div>
		            
		            <!-- right -->
		            <div class="col-xs-2">
		                <!-- manage button -->
		                @include('home_manage_button')
		            </div>

		        </div>

				@if ($tool['image'])
					<div class="col-xs-3">
						<img src="{!! $tool['image'] !!}" class="image">
					</div>
					<div class="col-xs-9">
				@else 
					<div class="col-xs-12">
				@endif 

						<!-- title -->
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-xs-12">
								<img src="/images/resource/tool.png" class="table_view_image"> <span class="pill bck-tools">{{ $tool['title'] }}</span>
							</div>
						</div>

						<!-- CONTENT --> 
						
						<!-- category -->
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-xs-2">
								{{ strtoupper(trans('text.category')) }}
							</div>
							<div class="col-xs-10">
								{{ $tool['category'] }}
							</div>
						</div>				

						<!-- description -->
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-xs-2">
								{{ strtoupper(trans('text.description')) }}
							</div>
							<div class="col-xs-10">
								{{ $tool['description'] }}
							</div>
						</div>		

						<!-- link -->
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-xs-2">
								{{ strtoupper(trans('text.file')) }}
							</div>
							<div class="col-xs-10">
								<a href="{{ $tool['filename'] }}">{{ $tool['filename'] }}</a>
							</div>
						</div>	

					</div>
			</div>

		</div>
	</div>


@endsection
