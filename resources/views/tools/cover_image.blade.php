<div class="image_container_inner" style="background-image: url({{ $tool->image }}); height: 200px; background-size: contain; background-repeat: no-repeat;">
	<!-- bottom popup -->
	<div class="image_change_bottom_cover bck-tools">
		<!-- remove image button -->
		<button class="btn btn-danger remove_tool_image_button" data-id="{!! $tool->id !!}" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
	</div>			
</div>