@extends('layouts.resource')

@section('content')

	<form method="POST" action="/tools/update/{!! $tool->id !!}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row bck-tools resource_header">
			<!-- left text -->
			<div class="col-xs-4 text-left resource_header_title">
				<table>
					<tr>
						<td style="height: 70px; vertical-align: middle; font-size: 38pt">
							<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-pencil" aria-hidden="true"></i></p>
						</td>
						<td style="vertical-align: middle; height: 70px; padding-left: 10px;">
							{{ strtoupper(trans('text.modify_mode')) }}
						</td>
					</tr>
				</table>
				
			</div>

			<!-- middle buttons -->
			<div class="col-xs-4">
				<table style="margin-left: auto; margin-right: auto;">
					<tr>
						<td style="padding: 5px;">
							<a href="/">
								<button class="btn btn-danger back-button" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
							</a>
							<br>
							{{ trans('text.back') }}
						</td>
					</tr>
				</table>			
			</div>
			
			<!-- right text -->
			<div class="col-xs-4 text-right resource_header_title">
				<table style="float: right;">
					<tr>
						<td style="vertical-align: middle; height: 70px;">
							{{ strtoupper(trans('text.tools')) }}
						</td>
						<td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
							<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-wrench" aria-hidden="true"></i></p>
						</td>
					</tr>
				</table>
				
			</div>			
		</div>
		

		<div class="container">
			<div class="row">

				<!-- LEFT PART - IMAGE -->
				<div class="col-xs-3">
					
					<!-- dropzone for cover image -->
					<div class="dropzeon dropzone_div" id="tool_cover_image_upload" style="background-image: url({{ $tool->image }}); height: 200px; background-size: contain; background-repeat: no-repeat; background-position: center;">
						<div class="image_container_inner">
							<span id="tool_cover_image_text"> 
								@if (!$tool->image)
									{{ trans('text.drop_or_click_to_select_image') }}
								@endif 
							</span>

							<!-- bottom popup -->
							<div class="image_change_bottom_cover bck-tools">
								<!-- remove image button -->
								<button class="btn btn-danger remove_tool_image_button" data-id="{!! $tool->id !!}" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
							</div>		
						</div>
					</div>

				</div>


				<!-- RIGHT PART - CONTENT -->
				<div class="col-xs-9">

					<!-- NAME -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-2">
							{{ strtoupper(trans('text.resource_title')) }}
						</div>
						<div class="col-xs-10">
							<input type="text" name="title" data-model='tools' data-id={{ $tool->id }} value="{{ $tool->title }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- CATEGORY -->
					<div class="row"  style="margin-top: 20px">
						<div class="col-xs-2">
							{{ strtoupper(trans('text.category')) }}
						</div>
						<div class="col-xs-10">
							<input type="text" name="category" list="categories" data-model='tools' data-id={{ $tool->id }} value="{{ $tool->category }}" placeholder="{{ strtoupper(trans('text.insert_category')) }}" class="form-control update_input" required>
							<datalist id="categories">
								@foreach ($category_list as $category) 
									<option value="{{ $category }}">
								@endforeach
							</datalist>
						</div>
					</div>

					<!-- DESCRIPTION -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-2">
							{{ strtoupper(trans('text.resource_title')) }}
						</div>
						<div class="col-xs-10">
							<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='tools' data-id={{ $tool->id }}>{{ $tool->description }}</textarea>
						</div>
					</div>


					<!-- FILE -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-2">
							{{ strtoupper(trans('text.file')) }}
						</div>
						<div class="col-xs-10">
							<div class="dropzeon dropzone_div" id="tool_upload">
								{!! ($tool->filename) ? $tool->filename : trans('text.click_to_select_file') !!}
							</div>
						</div>
					</div>
						
				</div>

			</div>
		</div>

	</form>



<script type="text/javascript">

	// DROPZONE - upload resouce and open resource modal
	$("#tool_upload").dropzone({ 
		url: "/tools/upload_file/{{ $tool->id }}",
		paramName: "filename",
		previewsContainer: false,
		uploadMultiple: false,
		autoDiscover: false,

		sending: function(file, xhr, formData) {
			formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
		},

		uploadprogress: function(file, progress, bytesSent) {
			// upload progress percentage
		    $("#tool_upload").html(parseInt(progress) + "%");
		},

		success: function(file, responce) {
			console.log('done');

			//add the row and open the modal
			if (responce != "upload_error") {
			
				$('#tool_upload').append(responce[1]);

			} else {

				alert ("{{ trans('text.upload_error') }}");
			}

			// Return text to "click here..."
			$("#tool_upload").html(responce);
			
		},

		// error return function
		error: function (file, responce){
			$("#tool_upload").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
		},

		init: function (){
			
			myDropzone = this;
		},

	});


	// DROPZONE COVER IMAGE
	$("#tool_cover_image_upload").dropzone({ 
		url: "/tools/upload_cover_image/{{ $tool->id }}",
		paramName: "image",
		previewsContainer: false,
		uploadMultiple: false,
		autoDiscover: false,

		sending: function(file, xhr, formData) {
			formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
		},

		uploadprogress: function(file, progress, bytesSent) {
			// upload progress percentage
		    $("#tool_cover_image_text").html(parseInt(progress) + "%");
		},

		success: function(file, responce) {

			// change the image on upload
			if (responce != "upload_error") {
				
				$('#tool_cover_image_upload').css("background-image", "url(" + responce + ")");  
				$('#tool_cover_image_text').html("");  

			} else {

				alert ("{{ trans('text.upload_error') }}");
			}

		},

		// error return function
		error: function (file, responce){
			$("#tool_upload").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
		},

		init: function (){
			
			myDropzone = this;
		},

	});




	$('.remove_tool_image_button').click( function(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        var id = $(this).data('id');
	    $.ajax({
            data: { 
                '_token': token,
            },
            url: "/tools/remove_cover_image/" + id,
            type: 'POST',

            success: function(response) {
                if (response) {

					$('#tool_cover_image_upload').css("background-image", "none");  
					$('#tool_cover_image_text').html("{{ trans('text.drop_or_click_to_select_image') }}");  

                } else {
					bootbox.alert (response);
                }
                
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
	});

	
    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}/tools/modify';
        return false;
    }); 

</script>

@endsection
