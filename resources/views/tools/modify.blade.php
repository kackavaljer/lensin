@extends('layouts.resource')

@section('content')


<div class="row bck-tools resource_header">
    <!-- left text -->
    <div class="col-xs-4 text-left resource_header_title">
        <table>
            <tr>
                <td style="height: 70px; vertical-align: middle; font-size: 38pt">
                    <p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-pencil" aria-hidden="true"></i></p>
                </td>
                <td style="vertical-align: middle; height: 70px; padding-left: 10px;">
                    {{ strtoupper(trans('text.modify_mode')) }}
                </td>
            </tr>
        </table>
        
    </div>

    <!-- middle buttons -->
    <div class="col-xs-4">
        <table style="margin-left: auto; margin-right: auto;">
            <tr>
                <td style="padding: 5px;">
                    <a href="{!! url('/') !!}"><button class="btn btn-danger back-button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button></a>
                    <br>
                    {{ strtoupper(trans('text.close')) }}
                </td>
            </tr>
        </table>            
    </div>
    
    <!-- right text -->
    <div class="col-xs-4 text-right resource_header_title">
        <table style="float: right;">
            <tr>
                <td style="vertical-align: middle; height: 70px;">
                    {{ strtoupper(trans('text.tools')) }}
                </td>
                <td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
                    <p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-wrench" aria-hidden="true"></i></p>                           
                </td>
            </tr>
        </table>
        
    </div>          
</div>



<div class="container">
	<div class="row">
		<div class="col-xs-12 text-center">		
	        <!-- DATATABLE SEARCH -->
		    <table class="display" id="datatable">
		        <thead>
		            <tr>
		                <th class="col-xs-1"></th>
		                <th class="col-xs-2">{{ trans('text.category') }}</th>
		                <th class="col-xs-3">{{ trans('text.title') }}</th>
		                <th class="col-xs-4">{{ trans('text.description') }}</th>
		                <th class="col-xs-2"></th>
		            </tr>
		        </thead>
		    </table>
		</div>
	</div>
</div>



<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/tools/datatable_list',
            rowId: 'id',
        },

        "columns": [
            { "data": "id" },
            { "data": "category" },
            { "data": "title" },
            { "data": "description" },
            { "data": "published" }
        ], 

		"aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {

            	var modify = '<a title="{{ trans('text.tooltip_modify_tool') }}" href="/tools/edit/' + row['id'] + '"><div class="btn bck-tools"><i class="fa fa-pencil" aria-hidden="true"></i></div></a> ';

            	if (row['published'] == 1) {
            		// if published
            		var publish = '<a title="{{ trans('text.tooltip_unpublish_tool') }}" href="/tools/unpublish/' + row['id'] + '"><div class="btn bck-tools"><i class="fa fa-eye-slash" aria-hidden="true"></i></div></a> ';
            	} else {
            		// if unpublished
            		var publish = '<a title="{{ trans('text.tooltip_publish_tool') }}" href="/tools/publish/' + row['id'] + '"><div class="btn bck-tools"><i class="fa fa-eye" aria-hidden="true"></i></div></a> ';
            	}

            	var delete_button = '<div class="btn btn-danger delete_tool" title="{{ trans('text.tooltip_delete_tool') }}" data-tool-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

            	return modify + publish + delete_button;
            },
            "aTargets": [ 4 ]
        }]

    });


    // 
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });


    $('body').on('click', '.delete_tool', function() {

        var tool_id = $(this).data('tool-id');
        // confirm the delete
        bootbox.confirm("{{ trans('text.question_delete_tool') }}", function(result){ 
            // if OK, delete the model
            if (result) {
                window.location.href = '/tools/delete/' + tool_id;
            }

        });

    });


</script>


@endsection