 @if (Auth::check() && server_property('central_server') != 1)

    <div class="round_button bck-course pull-right" id="manage_courses">
        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>

        <div class="resource_options">
            <table style="width:100%">

                <!-- MANAGE COURSES -->
                @if (Auth::user()->isRole('managecourses'))
                    <tr>
                        <td>
                            <a href="/courses/create/">
                                <button class="btn round_button course bck-course" id="edit_courses">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </a>
                        </td>
                        <td>
                            {{ strtoupper(trans('text.courses')) }} 
                        </td>
                        <td>
                            <a href="/courses/modify/">
                                <button class="btn round_button course bck-course" id="edit_courses">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </button>
                            </a> 
                        </td>
                    </tr>
                @endif


                <!-- MANAGE CASES -->
                @if (Auth::user()->isRole('managestudycases'))
                    <tr>
                        <td>
                            <a href="/study_cases/create/">
                                <button class="btn round_button course bck-study-case" id="edit_courses">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </a>    
                        </td>
                        <td>
                            {{ strtoupper(trans('text.study_cases')) }} 
                        </td>
                        <td>
                            <a href="/study_cases/modify/">
                                <button class="btn round_button course bck-study-case" id="edit_courses">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </button>
                            </a>                 
                        </td>
                    </tr>
                @endif

                <!-- MANAGE TOOLS -->
                @if (Auth::user()->isRole('managetools'))
                    <tr>
                        <td>
                            <a href="/tools/create/">
                                <button class="btn round_button tools bck-tools" id="create_tool">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </a>    
                        </td>
                        <td>
                            {{ strtoupper(trans('text.tools')) }} 
                        </td>
                        <td>
                            <a href="/tools/modify/">
                                <button class="btn round_button tools bck-tools" id="edit_tools">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </button>
                            </a>                 
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>

    <script>
        $('#manage_courses').click(function(){ 
            $(this).find('.resource_options').toggle();
        })
    </script>
@endif 