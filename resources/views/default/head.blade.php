    <!-- FAVICON -->
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

    <!-- JAVASCRIPT -->
        <!-- Jquery, Bootstrap, Vue, Sizzle, Datatalbes-->
         <script src="/js/app.js"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/jquery.dataTables.min.js"></script>
        <script src="/js/jquery.dataTables.yadcf.js"></script>
        <script src="/js/jquery.easing.min.js"></script>
        <script src="/js/jquery.scombobox.min.js"></script>
        <script src="/js/bootbox.min.js"></script>
        <script src="/js/lens.js"></script>
        <script src="/js/video-js/video.js"></script>

    <!-- CSS -->
        <!-- jquey ui 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
        -->
        
        <!-- Font awsome -->
        <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"/>    
        
        <!-- Bootstrap -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/bootstrap-theme.min.css" rel="stylesheet">

        <!-- Datatables --> 
        <link href="/css/jquery.dataTables.min.css" rel="stylesheet">

        <!-- Simle combo box -->
        <link href="/css/jquery.scombobox.min.css" rel="stylesheet">
        
        <!-- Custom Lens CSS -->
        <link href="/css/lens.css" rel="stylesheet">

        <!-- DROPZONE -->
        <link href="/css/dropzone.css" rel="stylesheet">

        <!-- Ubuntu font --> 
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">

        <!-- Video js -->
        <link href="/js/video-js/video-js.css" rel="stylesheet">


@if ( $central_server_list = central_server_data('/api/server_list'))
    
    <?php $platform_list = json_decode($central_server_list); ?>

    @if (isset($platform_list)) 
        <style>
            @foreach($platform_list as $platform)   
                input:checked + .slider_platform.round.platform_{!! $platform->id !!}_slider:before {
                    background-color: rgb(31,96,148); 
                    background-image: url("{!! $platform->address !!}/images/server/logo.png"); 
                    background-size: 100%;
                }
                .slider_platform.round.platform_{!! $platform->id !!}_slider:before {
                    background-image: url("{!! $platform->address !!}/images/server/logo_transparent.png");
                    background-size: 100%;
                }
            @endforeach
        </style>
    @endif

@endif