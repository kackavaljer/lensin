<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <img src="/images/erasmus.png"  style="height: 40px">
            </div>
            <div class="col-xs-6 text-center">
                {!! social_icons() !!}
            </div>
            <div class="col-xs-3 text-right">
                <img src="/images/creative-commons.png">
            </div>
        </div>
    </div>
</footer>