@extends('layouts.resource')

@section('content')

	<form method="POST" action="/projects/themes/create" enctype="multipart/form-data">
		{{ csrf_field() }}

		<!-- HEADER --> 
		<div class="row bck-project resource_header">
			<!-- left text -->
			<div class="col-xs-4 text-left resource_header_title">
				<table>
					<tr>
						<td style="height: 70px; vertical-align: middle; font-size: 38pt">
							<p style="line-height: 38pt; margin-bottom: 6px;">+</p>
						</td>
						<td style="vertical-align: middle; height: 70px; padding-left: 10px;">
							{{ strtoupper(trans('text.create_mode')) }}
						</td>
					</tr>
				</table>
				
			</div>

			<!-- middle buttons -->
			<div class="col-xs-4">
				<table style="margin-left: auto; margin-right: auto;">
					<tr>
						
						<td style="padding: 5px;">
							<button class="btn btn-success" type="submit"><i class="fa fa-check" aria-hidden="true"></i></button>
							<br>
							{{ strtoupper(trans('text.confirm')) }}
						</td>
						<td style="padding: 5px;">
							<button class="btn btn-danger back-button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
							<br>
							{{ strtoupper(trans('text.cancel')) }}
						</td>
					</tr>
				</table>			
			</div>
			
			<!-- right text -->
			<div class="col-xs-4 text-right resource_header_title">
				<table style="float: right;">
					<tr>
						<td style="vertical-align: middle; height: 70px;">
							{{ strtoupper(trans('text.project_theme')) }}
						</td>
						<td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
							<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-file-text-o" aria-hidden="true"></i></p>
						</td>
					</tr>
				</table>
				
			</div>			
		</div>
		

		<!-- CONTENT --> 
		<div class="container">

			<!-- TITLE -->
			<div class="row">
				<div class="col-xs-12 text-center">
					<h3>{{ trans('text.create_theme') }}</h3>
				</div>
			</div>

			<!-- NAME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.theme_name')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="name" class="form-control" placeholder="{{ strtoupper(trans('text.insert_name')) }}" required>
				</div>
			</div>

			<!-- DESCRIPTION -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.description')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control" placeholder="{{ strtoupper(trans('text.description')) }}"></textarea>
				</div>
			</div>
			
		</div>

	</form>



<script type="text/javascript">
	
    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}';
        return false;
    }); 

</script> 

@endsection
