@extends('layouts.resource')

@section('content')

{{ csrf_field() }}
<div class="row bck-project resource_header">
	<!-- left text -->
	<div class="col-xs-4 text-left resource_header_title">
		<table>
			<tr>
				<td style="height: 70px; vertical-align: middle; font-size: 38pt">
					<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-pencil" aria-hidden="true"></i></p>
				</td>
				<td style="vertical-align: middle; height: 70px; padding-left: 10px;">
					{{ strtoupper(trans('text.modify_mode')) }}
				</td>
			</tr>
		</table>
		
	</div>

	<!-- middle buttons -->
	<div class="col-xs-4">
		<table style="margin-left: auto; margin-right: auto;">
			<tr>
				<td style="padding: 5px;">
					<a href="/">
						<button class="btn btn-danger back-button" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					</a>
					<br>
					{{ trans('text.back') }}
				</td>
			</tr>
		</table>			
	</div>
	
	<!-- right text -->
	<div class="col-xs-4 text-right resource_header_title">
		<table style="float: right;">
			<tr>
				<td style="vertical-align: middle; height: 70px;">
					{{ strtoupper(trans('text.projects')) }}
				</td>
				<td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
					<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-file-text-o" aria-hidden="true"></i></p>
				</td>
			</tr>
		</table>
	</div>				
</div>


<div class="container">
	<div class="row">
		<div class="col-xs-12 text-center">
			<h2>{{ trans('text.edit_theme') }}</h2>
		</div>
	</div>

	<div class="row">

		<!-- CONTENT -->
		<div class="col-xs-12">

			<!-- NAME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.theme_name')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="name" data-model='projects/themes' data-id={{ $theme->id }} value="{{ $theme->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
				</div>
			</div>


			<!-- DESCRIPTION -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.resource_title')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='projects/themes' data-id={{ $theme->id }}>{{ $theme->description }}</textarea>
				</div>
			</div>
			
		</div>

	</div>
</div>


<script type="text/javascript">

    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}/projects/themes/modify';
        return false;
    }); 

</script>

@endsection
