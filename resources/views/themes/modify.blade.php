@extends('layouts.resource')

@section('content')


<div class="row bck-project resource_header">
    <!-- left text -->
    <div class="col-xs-4 text-left resource_header_title">
        <table>
            <tr>
                <td style="height: 70px; vertical-align: middle; font-size: 38pt">
                    <p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-pencil" aria-hidden="true"></i></p>
                </td>
                <td style="vertical-align: middle; height: 70px; padding-left: 10px;">
                    {{ strtoupper(trans('text.modify_mode')) }}
                </td>
            </tr>
        </table>
        
    </div>

    <!-- middle buttons -->
    <div class="col-xs-4">
        <table style="margin-left: auto; margin-right: auto;">
            <tr>
                <td style="padding: 5px;">
                    <a href="{!! url('/') !!}/projects/themes/create"><button class="btn btn-success back-button" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button></a>
                    <br>
                    {{ strtoupper(trans('text.new')) }}
                </td>
                <td style="padding: 5px;">
                    <a href="{!! url('/') !!}"><button class="btn btn-danger back-button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button></a>
                    <br>
                    {{ strtoupper(trans('text.close')) }}
                </td>
            </tr>
        </table>            
    </div>
    
    <!-- right text -->
    <div class="col-xs-4 text-right resource_header_title">
        <table style="float: right;">
            <tr>
                <td style="vertical-align: middle; height: 70px;">
                    {{ strtoupper(trans('text.projects')) }}
                </td>
                <td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
                    <p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-file-text-o" aria-hidden="true"></i></p>                           
                </td>
            </tr>
        </table>
        
    </div>          
</div>



<div class="container">
    <!-- TITLE  --> 
    <div class="row">
        <div class="col-xs-12 text-center">
            <h2>{{ trans('text.themes') }}</h2>
        </div>
    </div>

    <!-- THEMES DATATABLE -->
	<div class="row">
		<div class="col-xs-12 text-center">		
	        <!-- DATATABLE SEARCH -->
		    <table class="display" id="datatable">
		        <thead>
		            <tr>
		                <th class="col-xs-1"></th>
		                <th class="col-xs-2">{{ trans('text.name') }}</th>
		                <th class="col-xs-3">{{ trans('text.description') }}</th>
		                <th class="col-xs-2"></th>
		            </tr>
		        </thead>
		    </table>
		</div>
	</div>
</div>



<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/projects/themes/data_table_modify',
            rowId: 'id',
        },

        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "description" }
        ], 

		"aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {

            	var modify = '<a title="{{ trans('text.tooltip_modify_theme') }}" href="/projects/themes/edit/' + row['id'] + '"><div class="btn bck-project"><i class="fa fa-pencil" aria-hidden="true"></i></div></a> ';
                
                /* PUBLISH IF NEEDED LATER
            	if (row['published'] == 1) {
            		// if published
            		var publish = '<a title="{{ trans('text.tooltip_unpublish_theme') }}" href="/projects/themes/unpublish/' + row['id'] + '"><div class="btn bck-projects"><i class="fa fa-eye-slash" aria-hidden="true"></i></div></a> ';
            	} else {
            		// if unpublished
            		var publish = '<a title="{{ trans('text.tooltip_publish_theme') }}" href="/projects/themes/publish/' + row['id'] + '"><div class="btn bck-projects"><i class="fa fa-eye" aria-hidden="true"></i></div></a> ';
            	}
                */
               
            	var delete_button = '<div class="btn btn-danger delete_theme" title="{{ trans('text.tooltip_delete_theme') }}" data-theme-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

            	return modify + delete_button;
            },
            "aTargets": [ 3 ]
        }]

    });


    // 
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });


    $('body').on('click', '.delete_theme', function() {

        var theme_id = $(this).data('theme-id');
        // confirm the delete
        bootbox.confirm("{{ trans('text.question_delete_theme') }}", function(result){ 
            // if OK, delete the model
            if (result) {
                window.location.href = '/projects/themes/delete/' + theme_id;
            }
        });

    });


</script>


@endsection