@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <h2>{{ trans('text.join_as_participant') }}</h2>
            <br><br>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('./register') }}">
                {{ csrf_field() }}
                <!-- name -->
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">{{ trans('text.name') }}*</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- last_name -->
                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="last_name" class="col-md-4 control-label">{{ trans('text.last_name') }}*</label>

                    <div class="col-md-6">
                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- user type -->
                <div class="form-group{{ $errors->has('user_type_id') ? ' has-error' : '' }}">
                    <label for="user_type_id" class="col-md-4 control-label">{{ trans('text.user_type') }}*</label>

                    <div class="col-md-6">
                        {!! Form::select('user_type_id', $user_types, 1, ['required' => 'required', 'class' => 'form-control'] ) !!}

                        @if ($errors->has('user_type_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user_type_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">{{ trans('text.email') }}*</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- password -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">{{ trans('text.password') }}*</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- password confirm-->
                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">{{ trans('text.confirm_password') }}*</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
                <!-- univeristy / institution / company -->
                <div class="form-group{{ $errors->has('school') ? ' has-error' : '' }}">
                    <label for="school" class="col-md-4 control-label">{{ trans('text.univeristy_institution_company') }}*</label>

                    <div class="col-md-6">
                        <input id="school" type="text" class="form-control" name="school" value="{{ old('school') }}" required>

                        @if ($errors->has('school'))
                            <span class="help-block">
                                <strong>{{ $errors->first('school') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- departement -->
                <div class="form-group">
                    <label for="departement" class="col-md-4 control-label">{{ trans('text.departement') }}</label>

                    <div class="col-md-6">
                        <input id="departement" type="text" class="form-control" name="departement" value="{{ old('departement') }}" >
                    </div>
                </div>
                <!-- position -->
                <div class="form-group">
                    <label for="position" class="col-md-4 control-label">{{ trans('text.position') }}</label>

                    <div class="col-md-6">
                        <input id="position" type="text" class="form-control" name="position" value="{{ old('position') }}" >
                    </div>
                </div>
                <!-- country -->
                <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                    <label for="country_id" class="col-md-4 control-label">{{ trans('text.country') }}*</label>

                    <div class="col-md-6">
                        {!! Form::select('country_id', $countries, null, ['required' => 'required', 'class' => 'form-control', 'placeholder' => trans('text.select_country')] ) !!}

                        @if ($errors->has('country_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('country_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- address -->
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label for="address" class="col-md-4 control-label">{{ trans('text.address') }}*</label>

                    <div class="col-md-6">
                        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required>

                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <!-- interest -->
                <div class="form-group">
                    <label for="interest" class="col-md-4 control-label">{{ trans('text.research_interest') }}</label>

                    <div class="col-md-6">
                        <input id="interest" type="text" class="form-control" name="interest" value="{{ old('interest') }}" >
                    </div>
                </div>
                <!-- phone -->
                <div class="form-group">
                    <label for="phone" class="col-md-4 control-label">{{ trans('text.phone') }}</label>

                    <div class="col-md-6">
                        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" >
                    </div>
                </div>
                <!-- website -->
                <div class="form-group">
                    <label for="web" class="col-md-4 control-label">{{ trans('text.web') }}</label>

                    <div class="col-md-6">
                        <input id="web" type="text" class="form-control" name="web" value="{{ old('web') }}" >
                    </div>
                </div>
                <!-- RECAPCHA -->
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                       <!-- <div class="g-recaptcha" data-sitekey="{!! server_property('google_recapcha_key') !!}"></div> -->
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

@endsection
