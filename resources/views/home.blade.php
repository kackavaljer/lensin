@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<!-- MENU LEFT -->
			<div class="col-xs-2">
				@include('left_menu_filter', ['home_page' => 1])
			</div>

			<!-- CONTENT -->
			<div class="col-xs-10">

                <!-- content header -->
                <div class="row content-header">
                    
                    <!-- left button -->
                    <div class="col-xs-2">
                    </div>
                    
                    <!-- middle -->
                    <div class="col-xs-8">
                    </div>
                    
                    <!-- right -->
                    <div class="col-xs-2">
                        <!-- manage button -->
                        @include('home_manage_button')
                       
                    </div>

                </div>

                <!-- DATATABLE SEARCH -->
			    <table class="display home_datatable" id="datatable">
			        <thead>
			            <tr>
			                <th></th>
			                <th>{{ strtoupper(trans('text.title')) }}</th>
			                <th>{{ strtoupper(trans('text.author')) }}</th>
			                <th>{{ strtoupper(trans('text.country')) }}</th>
			                <th>{{ strtoupper(trans('text.year')) }}</th>
                            <th>{{ strtoupper(trans('text.language')) }}</th>
                            <th>type</th>
                            <th>platform</th>
			                <th>server_id</th>
			            </tr>
			        </thead>
			    </table>

			</div>
		</div>
	</div>

<script>

	var values = [];
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": false, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            "cache":false,
            "rowId": 'id',
            @if (server_property('central_server') == 1)
                "url" : '/all_resources_all_servers_datatables',
            @else 
                "url" : '/data_all_resources',
            @endif
        },

        "initComplete": function () {
            @if (isset($_GET['platforms']))
                @foreach ($_GET['platforms'] as $platform)
                    $( "#platform_{!! $platform !!}" ).trigger( "click" );
                @endforeach
            @endif
            
                var resources = "";

                $('.resources:checkbox:checked').each(function() {
                    resources += $(this).val() + "|";
                });

                // remove the last "|"
                resources = resources.substring(0, resources.length - 1);

                // search the data and redraw table
                table.column( 6 ).search(resources, true).draw();
        }, 

		"aoColumnDefs": [
            {
                "mRender": function ( data, type, row ) {
                    return "<span style='display:none'>" + row[6] + "</span><div class='text-center' style='width:100%'><img src='images/resource/" + row[6] + ".png' class='table_view_image'><br><img src='http://" +  row[7] + "/images/server/logo.png' class='table_view_image platform'></div>";
                },
                "aTargets": [ 0 ]
            },
            {
                "aTargets": [ 6, 7, 8 ],
                "visible": false
            },
     	],

    });


    // Reload table (not used for now)
	function reloadTable() {
		$("#datatable").DataTable().ajax.reload();
	}


    // IF CENTRAL SERVER, OPEN API VIEWS
    @if (server_property('central_server') == 1)
        // Open course on click
        $('#datatable tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
            location.href = '/' + data[6] + 's/view/' + data[0] + '?server_id=' + data[8];
        });
    @else 
        // Open course on click
        $('#datatable tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
            location.href = '/' + data[6] + 's/view/' + data[0];
        });
    @endif

</script>
@endsection

