@extends('layouts.resource')

@section('content')

{{ csrf_field() }}
<div class="row bck-project resource_header">
	<!-- left text -->
	<div class="col-xs-4 text-left resource_header_title">
		<table>
			<tr>
				<td style="height: 70px; vertical-align: middle; font-size: 38pt">
					<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-pencil" aria-hidden="true"></i></p>
				</td>
				<td style="vertical-align: middle; height: 70px; padding-left: 10px;">
					{{ strtoupper(trans('text.modify_mode')) }}
				</td>
			</tr>
		</table>
		
	</div>

	<!-- middle buttons -->
	<div class="col-xs-4">
		<table style="margin-left: auto; margin-right: auto;">
			<tr>
				<td style="padding: 5px;">
					<a href="/">
						<button class="btn btn-danger back-button" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					</a>
					<br>
					{{ trans('text.back') }}
				</td>
			</tr>
		</table>			
	</div>
	
	<!-- right text -->
	<div class="col-xs-4 text-right resource_header_title">
		<table style="float: right;">
			<tr>
				<td style="vertical-align: middle; height: 70px;">
					{{ strtoupper(trans('text.projects')) }}
				</td>
				<td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
					<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-file-text-o" aria-hidden="true"></i></p>
				</td>
			</tr>
		</table>
	</div>				
</div>


<div class="container">

	<!-- TITLE --> 
	<div class="row">
		<div class="col-xs-12 text-center">
			<h2>{{ trans('text.edit_challenge') }}</h2>
		</div>
	</div>	

	<!-- CONTENT -->

	<!-- THEME --> 
	<div class="row">
		<div class="col-xs-2">
			{{ strtoupper(trans('text.theme')) }}
		</div>
		<div class="col-xs-10">
			{!! Form::select('theme_id', $themes, $challenge->theme_id, ['class' => 'form-control update_input', 'data-model' => 'projects/challenge', 'data-id' => $challenge->id ]) !!}
		</div>
	</div>

	<!-- NAME -->
	<div class="row" style="margin-top: 20px">
		<div class="col-xs-2">
			{{ strtoupper(trans('text.name')) }}
		</div>
		<div class="col-xs-10">
			<input type="text" name="name" data-model='projects/challenge' data-id={{ $challenge->id }} value="{{ $challenge->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
		</div>
	</div>

	<!-- COURSE -->
	<div class="row" style="margin-top: 20px">
		<div class="col-xs-2">
			{{ strtoupper(trans('text.course')) }}
		</div>
		<div class="col-xs-10">
			<input type="text" name="course" data-model='projects/challenge' data-id={{ $challenge->id }} value="{{ $challenge->course }}" placeholder="{{ strtoupper(trans('text.course')) }}" class="form-control update_input" required>
		</div>
	</div>

	<!-- YEAR -->
	<div class="row" style="margin-top: 20px">
		<div class="col-xs-2">
			{{ strtoupper(trans('text.year')) }}
		</div>
		<div class="col-xs-10">
			<input type="text" name="year" id="year" list="years" class="form-control" value="{{ $challenge->year }}" placeholder="{{ strtoupper(trans('text.insert_course_name')) }}" required>
			<datalist id="years">
				@foreach (range(date('Y'), 1970) as $year) 
					<option value="{{ $year }}">
				@endforeach
			</datalist>
		</div>
	</div>

	<!-- DESCRIPTION -->
	<div class="row" style="margin-top: 20px">
		<div class="col-xs-2">
			{{ strtoupper(trans('text.resource_title')) }}
		</div>
		<div class="col-xs-10">
			<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='projects/challenges' data-id={{ $challenge->id }}>{{ $challenge->description }}</textarea>
		</div>
	</div>
			

	</div>
</div>


<script type="text/javascript">

    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}/projects/challenges/modify';
        return false;
    }); 

</script>

@endsection
