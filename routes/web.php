<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// AUTH
	Auth::routes();
	Route::get('/logout', 'Auth\LoginController@logout');

// HOME
	Route::get('/', function () { return view('home'); });

// RESOURCES 
	Route::get('/data_all_resources', 'DataController@all_resources_datatable');
	Route::get('/all_resources', 'DataController@all_resources_array');
	Route::get('/all_resources_all_servers', 'DataController@all_resources_all_servers');
	Route::get('/all_resources_all_servers_datatables', 'DataController@all_resources_all_servers_datatables');


// FAKE DATA RETURN
	Route::get('/data', 'FakedataController@data');
	Route::post('/data', 'FakedataController@data');
	
// COURSES
	Route::get('/courses/view/{course_id}', 'CourseController@view');
	Route::get('/courses/view/api/{course}', 'CourseController@view_api');
	Route::get('/courses/datatable_list', 'CourseController@datatable_list');
	Route::post('/courses/update_image/{course}', 'CourseController@update_image');
	//edit
	$router->get('/courses/edit/{course}', ['as' => '/courses/edit/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@edit' ]);
	$router->post('/courses/edit/view/list/{course}', ['as' => '/courses/edit/view/list/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@edit_list_view' ]);
	$router->post('/courses/edit/view/grid/{course}', ['as' => '/courses/edit/view/grid/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@edit_grid_view' ]);
	
	$router->get('/courses/delete/{course}', ['as' => '/courses/delete/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@delete' ]);
	//$router->get('/courses/create', ['as' => '/courses/create', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@createPage' ]);
	$router->get('/courses/create', ['as' => '/courses/create', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@create' ]);
	$router->post('/courses/update_data', ['as' => '/courses/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@update_data' ]);
	$router->get('/courses/update_data', ['as' => '/courses/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@update_data' ]);
	$router->get('/courses/modify', ['as' => '/courses/modify', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@modify' ]);
	$router->get('/courses/publish/{course}', ['as' => '/courses/publish/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@publish' ]);
	$router->get('/courses/unpublish/{course}', ['as' => '/courses/unpublish/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@unpublish' ]);
	$router->post('/courses/add_teacher', ['as' => '/courses/add_teacher', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@add_teacher' ]);
	$router->post('/courses/remove_teacher', ['as' => '/courses/remove_teacher', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@remove_teacher']);
	$router->post('/courses/remove_image/{course}', ['as' => '/courses/remove_image/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@remove_image']);

// COURSE SUBJECTS
	$router->get('/course_subjects/create/{course}', ['as' => '/course_subjects/create/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@create' ]);
	$router->post('/course_subjects/delete', ['as' => '/course_subjects/delete', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@delete' ]);
	$router->post('/course_subjects/update_data', ['as' => '/course_subjects/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@update_data' ]);
	$router->get('/course_subjects/update_data', ['as' => '/course_subjects/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@update_data' ]);
	$router->post('/course_subjects/publish', ['as' => '/course_subjects/publish', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@publish' ]);
	$router->post('/course_subjects/update_sort', ['as' => '/course_subjects/update_sort', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@update_sort' ]);

// LECTURES
	Route::get('/lectures/view/{lecture_id}', 'LectureController@view');
	Route::get('/lectures/view/api/{lecture}', 'LectureController@view_api');
	$router->get('/lectures/create/{course_subject}', ['as' => '/lectures/create/{course_subject}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@create' ]);
	$router->post('/lectures/update_data', ['as' => '/lectures/update_data', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@update_data' ]);
	$router->post('/lectures/delete', ['as' => '/lectures/delete', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@delete' ]);
	$router->post('/lectures/upload_cover_image/{lecture}', ['as' => '/lectures/upload_cover_image/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@upload_cover_image' ]);
	$router->post('/lectures/publish', ['as' => '/lectures/publish', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@publish' ]);
	$router->post('/lectures/update_sort', ['as' => '/lectures/update_sort', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@update_sort' ]);
	$router->post('/lectures/edit_modal/{lecture}', ['as' => '/lectures/edit_modal/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@edit_modal' ]);
	$router->post('/lectures/grid_view_edit/{lecture}', ['as' => '/lectures/grid_view/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@grid_view_edit' ]);
	$router->get('/lectures/grid_view_edit/{lecture}', ['as' => '/lectures/grid_view_edit/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@grid_view_edit' ]);
	
	// lecture content	
	$router->post('/lectures/content/create/{lecture}', ['as' => '/lectures/content/create/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@create' ]);
	$router->post('/lectures/content/delete/{lectureContent}', ['as' => '/lectures/content/delete/{lectureContent}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@delete' ]);
	$router->post('/lectures/content/update_data', ['as' => '/lectures/content/update_data', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@update_data' ]);
	$router->post('/lectures/content/load_edit/{lecture}', ['as' => '/lectures/content/load_edit/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@load_edit' ]);
	$router->get('/lectures/content/load_edit/{lecture}', ['as' => '/lectures/content/load_edit/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@load_edit' ]);


// TOOLS
 	Route::get('/tools/datatable_list', 'ToolController@datatable_list');
	Route::get('/tools/view/{tool_id}', 'ToolController@view');
	Route::get('/tools/view/api/{tool}', 'ToolController@view_api');

	$router->get('/tools/create', ['as' => '/tools/create', 'middleware' => 'role:managetools', 'uses' => 'ToolController@create_view' ]);
	$router->post('/tools/create/', ['as' => '/tools/create/', 'middleware' => 'role:managetools', 'uses' => 'ToolController@create' ]);
	$router->get('/tools/modify', ['as' => '/tools/modify', 'middleware' => 'role:managetools', 'uses' => 'ToolController@modify' ]);
	$router->get('/tools/edit/{tool}', ['as' => '/tools/edit/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@edit' ]);
	$router->get('/tools/publish/{tool}', ['as' => '/tools/publish/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@publish' ]);
	$router->get('/tools/unpublish/{tool}', ['as' => '/tools/unpublish/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@unpublish' ]);
	$router->post('/tools/update_data', ['as' => '/tools/update_data', 'middleware' => 'role:managetools', 'uses' => 'ToolController@update_data' ]);
	$router->post('/tools/upload_file/{tool}', ['as' => '/tools/upload_file/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@upload_file' ]);
	$router->get('/tools/delete/{tool}', ['as' => '/tools/delete/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@delete' ]);
	$router->post('/tools/upload_cover_image/{tool}', ['as' => '/tools/upload_cover_image/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@upload_cover_image' ]);
	$router->post('/tools/remove_cover_image/{tool}', ['as' => '/tools/remove_cover_image/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@remove_cover_image' ]);
	


// STUDY CASES
	Route::get('/study_cases/view/{study_case}', 'StudyCaseController@view');
	Route::get('/study_cases/view/api/{study_case}', 'StudyCaseController@view_api');
	$router->get('/study_cases/create/', ['as' => '/study_cases/create/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@createPage' ]);
	$router->post('/study_cases/create/', ['as' => '/study_cases/create/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@create' ]);
	$router->get('/study_cases/modify/', ['as' => '/study_cases/modify/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@modify' ]);
	$router->get('/study_cases/datatable_list', ['as' => '/study_cases/datatable_list/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@datatable_list' ]);
	$router->get('/study_cases/publish/{study_case}', ['as' => '/study_cases/publish/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@publish' ]);
	$router->get('/study_cases/unpublish/{study_case}', ['as' => '/study_cases/unpublish/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@unpublish' ]);
	$router->get('/study_cases/delete/{study_case}', ['as' => '/study_cases/delete/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@delete' ]);
	$router->get('/study_cases/edit/{study_case}', ['as' => '/study_cases/edit/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@edit' ]);
	$router->post('/study_cases/update_data', ['as' => '/study_cases/update_data', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@update_data' ]);
	$router->post('/study_cases/remove_image/{study_case}', ['as' => '/study_cases/remove_image/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@remove_image' ]);
	$router->post('/study_cases/cover_image_upload/{study_case}', ['as' => '/study_cases/cover_image_upload/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@cover_image_upload' ]);
	$router->post('/study_cases/report_upload/{study_case}', ['as' => '/study_cases/report_upload/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@report_upload' ]);
	$router->post('/study_cases/report_delete', ['as' => '/study_cases/report_delete', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@report_delete' ]);

	//guidelines
	$router->post('/study_case_guideline/load_guidelines', ['as' => '/study_case_guideline/load_guidelines', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@load_guidelines' ]);
	$router->get('/study_case_guideline/load_guidelines', ['as' => '/study_case_guideline/load_guidelines', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@load_guidelines' ]);
	$router->post('/study_case_guideline/create', ['as' => '/study_case_guideline/create', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@create' ]);
	$router->post('/study_case_guideline/update_data', ['as' => '/study_case_guideline/update_data', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@update_data' ]);
	$router->post('/study_case_guideline/delete/{study_case_guideline}', ['as' => '/study_case_guideline/delete/{study_case_guideline}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@delete' ]);
	$router->post('/study_case_guideline/assign/{study_case}', ['as' => '/study_case_guideline/assign/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@assign' ]);


// RESOURCES
	$router->get('/resources/create/{lecture}', ['as' => '/resource/create/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@create' ]);
	$router->post('/resources/create/{lecture}', ['as' => '/resource/create/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@create' ]);
	$router->post('/resources/upload_file/{resource}', ['as' => '/resource/upload_file/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@upload_file' ]);
	$router->post('/resources/create_and_upload_files/{lecture}', ['as' => '/resource/create_and_upload_files/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@create_and_upload_files' ]);
	$router->post('/resources/delete', ['as' => '/resources/delete', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@delete' ]);
	$router->post('/resources/remove_file/{resource}', ['as' => '/resources/remove_file/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@remove_file' ]);
	Route::get('/resources/edit_modal/{resource}', 'ResourceController@edit_modal');
	$router->post('/resources/update_data', ['as' => '/resources/update_data', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@update_data' ]);
	$router->post('/resources/licence_update', ['as' => '/resources/licence_update', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@licence_update' ]);
	$router->post('/resources/upload_file/{resource}', ['as' => '/resources/upload_file/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@upload_file' ]);
	$router->post('/resources/publish', ['as' => '/resources/publish', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@publish' ]);
	$router->post('/resources/lecture_modal_file_list/{resource}', ['as' => '/resources/lecture_modal_file_list/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@lecture_modal_file_list' ]);
	Route::post('/resources/view_modal/{resource_id}', 'ResourceController@view_modal');
	Route::get('/resources/view_modal/{resource_id}', 'ResourceController@view_modal');
	Route::get('/resources/view_modal/api/{resource_id}', 'ResourceController@view_modal_api');
	Route::post('/resources/view_modal/api/{resource_id}', 'ResourceController@view_modal_api');
	Route::post('/courses/resource/delete_file/{resource}', 'ResourceController@delete_file');


/***** PROJECTS ****/
// THEMES
	Route::get('/projects/themes/data_table_modify', 'ThemesController@data_table_modify');
	$router->get('/projects/themes/create', ['as' => '/projects/themes/create', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@create_view' ]);
	$router->post('/projects/themes/create', ['as' => '/projects/themes/create', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@create' ]);
	$router->get('/projects/themes/edit/{theme}', ['as' => '/projects/themes/edit/{theme}', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@edit' ]);
	$router->get('/projects/themes/delete/{theme}', ['as' => '/projects/themes/delete/{theme}', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@delete' ]);
	$router->get('/projects/themes/modify', ['as' => '/projects/themes/modify', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@modify' ]);
	$router->post('/projects/themes/update_data', ['as' => '/projects/themes/update_data', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@update_data' ]);

// CHALANGES
	Route::get('/projects/challenges/data_table_modify', 'ChallengesController@data_table_modify');
	$router->get('/projects/challenges/modify', ['as' => '/projects/challenges/modify', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@modify' ]);

	$router->get('/projects/challenges/create', ['as' => '/projects/challenges/create', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@create_view' ]);
	$router->post('/projects/challenges/create', ['as' => '/projects/challenges/create', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@create' ]);
	$router->get('/projects/challenges/edit/{challenge}', ['as' => '/projects/challenges/edit/{challenge}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@edit' ]);
	$router->get('/projects/challenges/delete/{challenge}', ['as' => '/projects/challenges/delete/{challenge}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@delete' ]);
	$router->post('/projects/challenges/update_data', ['as' => '/projects/challenges/update_data', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@update_data' ]);


/**** END PROJECTS ****/

// USERS
	Route::get('/users/list', 'UserController@datatable_list');
	$router->post('/backend/user_roles/update_data', ['as' => '/backend/users/update_data', 'middleware' => 'role:manageusers', 'uses' => 'UserController@update_roles']);
	// profile
	Route::get('/profile', 'UserController@profile')->middleware('auth');
	Route::post('/profile/upload_image/{user}', 'UserController@upload_image')->middleware('auth');
	Route::post('/profile/remove_image/{user}', 'UserController@remove_image')->middleware('auth');


// BACKEND
	Route::get('/backend', 'BackendController@backend')->middleware('auth');

	// register a platform
	Route::post('/backend/register_platform', 'BackendController@register_platform');
	Route::get('/backend/register_platform', 'BackendController@register_platform');
	// send registration to central server
	$router->get('/backend/register_platform_request', ['as' => '/backend/register_platform_request', 'middleware' => 'role:manageusers', 'uses' => 'BackendController@register_platform_request' ]);

	//users
	$router->get('/backend/users', ['as' => '/backend/users', 'middleware' => 'role:manageusers', 'uses' => 'BackendController@users_list' ]);
	$router->get('/backend/users/edit/{user}', ['as' => '/backend/users/edit/{user}', 'middleware' => 'role:manageusers', 'uses' => 'BackendController@user_edit' ]);
	$router->post('/backend/users/update_data', ['as' => '/backend/users/update_data', 'middleware' => 'role:manageusers', 'uses' => 'UserController@update_data_backend' ]);
	$router->post('/backend/user_roles/update/{user}', ['as' => '/backend/user_roles/update/{user}', 'middleware' => 'role:manageusers', 'uses' => 'UserController@update_roles' ]);
	
	// server properties
	$router->get('/backend/site', ['as' => '/backend/site', 'middleware' => 'role:managesite', 'uses' => 'BackendController@site' ]);
	$router->post('/backend/site', ['as' => '/backend/site', 'middleware' => 'role:managesite', 'uses' => 'BackendController@site_update' ]);
	$router->post('/backend/upload/{path}', ['as' => '/backend/upload/{path}', 'middleware' => 'role:managesite', 'uses' => 'BackendController@upload_logo' ]);

	//server list
	$router->get('/backend/servers', ['as' => '/backend/servers', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@index' ]);
	$router->post('/backend/servers/update/{server_list}', ['as' => '/backend/servers/update/{server_list}', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@update' ]);
	$router->post('/backend/servers/create', ['as' => '/backend/servers/create', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@create' ]);
	$router->get('/backend/servers/delete/{server_list}', ['as' => '/backend/servers/delete/{server_list}', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@delete' ]);

	// languages
	$router->get('/backend/languages', ['as' => '/backend/languages', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@index' ]);
	$router->post('/backend/languages/create', ['as' => '/backend/languages/create', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@create' ]);
	$router->post('/backend/languages/update/{language}', ['as' => '/backend/languages/update/{language}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@update' ]);
	$router->get('/backend/languages/delete/{language}', ['as' => '/backend//delete/{language}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@delete' ]);
	// translations
	$router->get('/backend/translations/{language}', ['as' => '/backend/translations/{language}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageTranslationsController@index' ]);
	$router->post('/backend/translations/update/{translation}', ['as' => '/backend/translations/update/{translation}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageTranslationsController@update' ]);
	
	Route::get('/backend/synchronize', 'BackendController@synchronize')->middleware('cors');

// LOCALE
	Route::get('/locale/{page}', 'LanguageController@change_locale');


// PAGES
	Route::get('/{page}', 'HomeController@empty_page');
	

// API
	Route::get('/api/server_list', 'ServerListController@server_list');
